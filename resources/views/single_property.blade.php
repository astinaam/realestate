@extends('layouts.app')

@section('content')
    {{-- trigger main --}}
    <script>
        // document.getElementsByTagName('main')[0].style.marginTop = '50px';
    </script>

    @php
     $images = json_decode($property->property_images);
     if(!$images || count($images) == 0){
         $images[] = "default_image.jpg";
     }
    @endphp

    <script>
        var the_title = "{!! $property->property_title !!}";
        var the_id = "{!! $property->property_id !!}";
        var id = "{!! $property->id !!}";
        var the_image_link = "{!! asset('storage/'.$images[0]) !!}";
    </script>

    <div class="row" style="margin-top:30px;">
        <div class="col-sm-9">
                <div class="property_title" 
                    style="background: {{ setting('site.color_secondary') }}; 
                    width:fit-content; text-align:center; 
                    margin:0 auto; color: {{ setting('site.color_accent') }}; 
                    padding: 10px; 
                    border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;  
                    box-shadow: 1px 2px 20px 10px {{ setting('site.color_accent') }};";>
                        <h3>{{ $property->property_title }}</h3>
                        <h5>{{ $property->property_location }}</h5>
                    </div>
        </div>
    </div>


    <style>
    .card-header{
        background-color: {{ setting('site.color_secondary') }};
        color: {{ setting('site.color_accent') }};
    }
    </style>

    <div class="main_section">
        <div class="row">
            <div class="col-sm-9 for_mobile">
                <div class="property_image">
                    {{-- lightbox css starts --}}
                    {{-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"> --}}
                    <link rel="stylesheet" href="{{ asset('css/lightbox.css') }}">
                    
                    {{-- lightbox css ends --}}

                    {{-- TODO : take care of thumbnail --}}
                    {{-- <a href="#lightbox" data-toggle="modal">
                            <img class="img-responsive hover-shadow cursor" 
                                src="{{ asset('storage/'.$images[0]) }}" 
                                alt="{{ $property->property_title }}"
                                title="Click to see all the images"
                            >
                        </a> --}}
                    
                    {{-- try outer carasouel --}}

                    <div id="myCarousel" class="outer-slide carousel slide" data-ride="carousel" data-interval="false">
                        {{-- indicators --}}
                        
                            @php
                                $firstImage = true;
                                $slides = 0;
                            @endphp
                            {{-- pathed without removing else. no need of else now --}}
                            {{-- @if(count($images) > 0) --}}
                            <ol class="right-side" id="myindicator">
                                @foreach ($images as $image)
                                    @if ($firstImage)
                                        <li onclick="makeActive(this);" data-target="#myCarousel" data-slide-to="{{ $slides }}" class="active" >
                                            <img src="{{ asset('storage/'.$image) }}" alt="Image {{ $slides+1 }}" title="Image {{ $slides+1 }}">
                                        </li>
                                        @php
                                            $firstImage = false;
                                        @endphp
                                    @else
                                        <li onclick="makeActive(this);" data-target="#myCarousel" data-slide-to="{{ $slides }}">
                                            <img src="{{ asset('storage/'.$image) }}" alt="Image {{ $slides+1 }}" title="Image {{ $slides+1 }}">
                                        </li>
                                    @endif
                                    @php
                                        $slides += 1;
                                    @endphp
                                @endforeach
                            </ol>
                            @php
                                $firstImage = true;
                                $slides = 0;
                            @endphp
                            {{-- @else --}}
                                <ol class="carousel-indicators onmobile-ind" style="display:none;">
                                    @foreach ($images as $image)
                                        @if ($firstImage)
                                            <li data-target="#myCarousel" data-slide-to="{{ $slides }}" title="Image {{ $slides+1 }}" class="active">
                                            </li>
                                            @php
                                                $firstImage = false;
                                            @endphp
                                        @else
                                            <li data-target="#myCarousel" data-slide-to="{{ $slides }}" title="Image {{ $slides+1 }}">
                                            </li>
                                        @endif
                                        @php
                                            $slides += 1;
                                        @endphp
                                    @endforeach
                                </ol>
                                {{-- item of 6 each do later --}}
                                {{-- <ol class="carousel-indicators inner-slide">
                                    <div id="indicator-slider" class="carousel slide" data-interval="false">
                                        <div class="carousel-inner">
                                            @php
                                                $done = 0;
                                                $threshold = 6;
                                                $length = count($images);
                                                $isActive = "active";
                                            @endphp
                                            @while ($done < $length)  
                                                <div class="item {{ $isActive }}" >
                                                    @for ($i = $done; $i < min($done + $threshold, $length); $i++)
                                                        <li data-target="#myCarousel"
                                                            data-slide-to="{{ $i }}">
                                                            <img src="{{ asset('storage/'.$images[$i]) }}" alt="Image {{ $i+1 }}" title="Image {{ $i+1 }}">
                                                        </li>
                                                    @endfor
                                                </div>
                                                @php
                                                    $isActive = "";
                                                    $done += $threshold;
                                                @endphp
                                            @endwhile
                                        </div>
                                        <a class="left carousel-control inner-control" href="#indicator-slider" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control inner-control" href="#indicator-slider" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div> --}}
                            </ol>
                            {{-- @endif --}}

                            <script>
                                // add & remove active class
                                function makeActive(el){
                                    var ind = document.getElementById('myindicator');
                                    for( var i=1; i < ind.childNodes.length; i+=2){
                                        // console.log(i, ind.childNodes[i]);
                                        if(ind.childNodes[i].classList.contains('active')){
                                            ind.childNodes[i].classList.remove('active');
                                        }
                                    }
                                    el.classList.add('active');
                                }
                            </script>
                        
                         <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            @php
                                $firstImage = true;
                                $slides = 0;
                            @endphp
                            @foreach ($images as $image)
                                @if ($firstImage)
                                    <div class="item active">
                                        <a href="#lightbox" data-toggle="modal">
                                            <img src="{{ asset('storage/'.$image) }}"
                                                class="img-responsive hover-shadow cursor"
                                                title="Click to enlarge"
                                                alt="{{ $property->property_title }}">
                                        </a>
                                    </div>
                                    @php
                                        $firstImage = false;
                                    @endphp
                                @else
                                    <div class="item">
                                        <a href="#lightbox" data-toggle="modal" data-slide-to="{{ $slides }}">
                                                <img src="{{ asset('storage/'.$image) }}"
                                                    class="img-responsive hover-shadow cursor"
                                                    title="Click to enlarge"
                                                    alt="{{ $property->property_title }}">
                                        </a>
                                        
                                    </div>
                                @endif
                                @php
                                    $slides += 1;
                                @endphp
                            @endforeach
                        </div>
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"><i class="fas fa-chevron-circle-left"></i></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"><i class="fas fa-chevron-circle-right"></i></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    

                    {{-- lightbox modal starts --}}
                    <div class="container">
                        
                        <div class="modal fade and carousel slide" id="lightbox" style="position:fixed;">
                            <div class="modal-dialog">
                            <div class="modal-content">
                                    <button type="button" 
                                    class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                <div class="modal-body">
                                <ol class="carousel-indicators">
                                    @php
                                        $firstImage = true;
                                        $slides = 0;
                                    @endphp
                                    @foreach ($images as $image)
                                        @if ($firstImage)
                                            <li data-target="#lightbox" data-slide-to="{{ $slides }}" class="active"></li>
                                            @php
                                                $firstImage = false;
                                            @endphp
                                        @else
                                            <li data-target="#lightbox" data-slide-to="{{ $slides }}"></li>
                                        @endif
                                        @php
                                            $slides += 1;
                                        @endphp
                                    @endforeach
                                </ol>
                                <div class="carousel-inner">
                                    @php
                                        $firstImage = true;
                                    @endphp
                                    @foreach ($images as $image)
                                        @if ($firstImage)
                                            <div class="item active">
                                                <img src="{{ asset('storage/'.$image) }}" alt="{{ $property->property_title }}">
                                                <div class="carousel-caption"><p>{{ $property->property_title }}</p></div>
                                            </div>
                                            @php
                                                $firstImage = false;
                                            @endphp
                                        @else
                                            <div class="item">
                                                <img src="{{ asset('storage/'.$image) }}" alt="{{ $property->property_title }}">
                                                <div class="carousel-caption"><p>{{ $property->property_title }}</p></div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div><!-- /.carousel-inner -->
                                <a class="left carousel-control" href="#lightbox" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"><i class="fas fa-chevron-circle-left"></i></span>
                                </a>
                                <a class="right carousel-control" href="#lightbox" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"><i class="fas fa-chevron-circle-right"></i></span>
                                </a>
                                </div><!-- /.modal-body -->
                            </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </div><!-- /.container -->
                    {{-- lightbox modal ends --}}

                    {{-- lightbox javascripts starts--}}
                    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
                    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
                    {{-- lightbox javascripts endss--}}
                    
                </div>
                @php
                    // classify what should display and what not
                    $display = array();
                    $all_display = array();
                    $state_array = array(
                        "property_type",
                        "property_status",
                        "property_city",
                        "property_location",
                        "property_bedrooms",
                        "property_bathrooms",
                        "property_price",
                        "property_square_meters"
                    );

                    $all_display['property_title'] = 1;
                    $all_display['property_description'] = 1;

                    $all_display['property_type'] = 1;
                    $all_display['property_status'] = 1;
                    $all_display['property_city'] = 1;
                    $all_display['property_location'] = 1;
                    $all_display['property_bedrooms'] = 1;
                    $all_display['property_bathrooms'] = 1;
                    
                    $all_display['property_price'] = 1;
                    $all_display['property_square_meters'] = 1;

                    $all_display['property_features'] = 1;

                    $all_display['property_additional_details'] = 1;
                    $all_display['property_video'] = 1;
                    $all_display['property_map'] = 1;

                    $extra_attributes = array();

                    $dont_display = $property->property_dont_display;
                    $extra = $property->property_extra;

                    // tokenize dont display
                    $dont_display = explode(",", $dont_display);
                    foreach ($dont_display as $value) {
                        $value = trim($value);

                        if(!empty($value) && array_key_exists($value, $all_display))
                        {
                            $all_display[$value] = 0;
                        }
                    }

                    foreach ($state_array as $value) {
                        if($all_display[$value] == 1 && !empty($property->{$value})){
                            $showas = ucwords(str_replace("_"," ",$value));
                            $display[$showas] = $property->{$value};
                        }
                    }

                    // tokenize extraas
                    $extra = explode(",", $extra);
                    foreach ($extra as $val) {
                        $val = explode(":", $val);

                        if(count($val) < 2) continue;

                        $key = trim($val[0]);
                        $value = trim($val[1]);

                        if(!empty($key) && !empty($value)){
                            $display[$key] = $value;
                        }
                    }



                @endphp
                <div class="property_description">
                    <div class="card" style="margin:30px; border-radius: 0;">
                        <div class="card-header">
                         <h3 style="font-size:24px; margin-bottom:0; color: {{ setting('site.color_accent')  }};">{{ $property->property_title }}
                                    <span class="badge " style="background-color:{{ setting('site.color_accent') }}; color:black;"> 
                                            {{ $property->property_status }}
                                        </span>
                                        <span class="badge " style="background-color:{{ setting('site.color_accent') }}; color:black;"> 
                                            &euro;{{ number_format($property->property_price, 2) }}
                                        </span>

                            </h3>
                        </div>
                        <div class="card-body">
                                
                                <div class="property_distances">
                                    @if ($property->property_dist_from_airport)
                                        <li>
                                            <div class="dist_item airport" 
                                                title="{{$property->property_dist_from_airport}} kilometers away from airport"
                                                style="background-image:url({{ asset('images/distance/airport.svg') }});"></div>
                                            <span>{{$property->property_dist_from_airport}} Km</span>
                                        </li>
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                    @endif
                                    @if ($property->property_dist_from_sea)
                                        <li>
                                            <div class="dist_item sea" 
                                                title="{{$property->property_dist_from_sea}} kilometers away from sea"
                                                style="background-image:url({{ asset('images/distance/sea.svg') }});"></div>
                                            <span>{{$property->property_dist_from_sea}} Km</span>
                                        </li>
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                    @endif
                                    @if ($property->property_dist_from_port)
                                        <li>
                                            <div class="dist_item port" 
                                                title="{{$property->property_dist_from_port}} kilometers away from port"
                                                style="background-image:url({{ asset('images/distance/port.svg') }});"></div>
                                            <span>{{$property->property_dist_from_port}} Km</span>
                                        </li>
                                    @endif
                                </div>
                            @if ($all_display['property_description'] == 1 && !empty($property->property_description))
                            <h4 class="card-title">Description</h4>
                                {!! $property->property_description !!}
                            @endif
                            
                        </div>
                    </div>
                    <div class="card" style="margin:30px; border-radius: 0;">
                        <div class="card-header" style="color: {{ setting('site.color_accent') }};"><strong>Propery Details</strong></div>
                        <div class="card-body">
                                <div class="row">
                                    @php
                                        $cnt = 0;
                                    @endphp
                                    @foreach ($display as $key => $item)
                                        @if ($cnt % 6 == 0 && $cnt > 0)
                                            </table> </div> </div>
                                        @endif
                                        @if ($cnt % 6 == 0 || $cnt == 0)
                                            <div class="col-sm-6">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered  table-hover padding-control">

                                        @endif
                                            <tr>
                                                <td>
                                                    {{ $key }}
                                                </td>
                                                <td>
                                                    @if (strpos($key, 'Price') !== false)
                                                        &euro;{{ number_format($item, 2) }}
                                                    @elseif(strpos($key, 'City') !== false)
                                                        <a class="hoverme" style="text-decoration:none; color:black; display:inline-block;"
                                                            title="Click to search for properties in this city!"
                                                            href="{{ url('properties?property_city='.$item) }}">
                                                            {{ $item }}
                                                        </a>
                                                    @elseif(strpos($key, 'Location') !== false)
                                                        <a class="hoverme" style="text-decoration:none; color:black; display:inline-block;"
                                                            title="Click to search for properties in this location!"
                                                            href="{{ url('properties?property_location='.$item) }}">
                                                            {{ $item }}
                                                        </a>
                                                    @elseif(strpos($key, 'Status') !== false || strpos($key, 'Type') !== false)
                                                        <a class="hoverme" style="text-decoration:none; color:black; display:inline-block;"
                                                            title="Click to search for '{{ $item }}' properties!"
                                                            href="{{ url('properties?property_'.strtolower(explode(" ",$key)[1]).'='.$item) }}">
                                                            {{ $item }}
                                                        </a>
                                                    @elseif(strpos($key, 'rooms') !== false && strpos($key, 'Property') !== false)
                                                        <a class="hoverme" style="text-decoration:none; color:black; display:inline-block;"
                                                            title="Click to search for properties with {{ $item." or more ".explode(" ",$key)[1] }}!"
                                                            href="{{ url('properties?property_'.strtolower(explode(" ",$key)[1]).'='.$item) }}">
                                                            {{ $item }}
                                                        </a>
                                                    @else
                                                        {{ $item }}
                                                    @endif

                                                    
                                                    
                                                </td>
                                            </tr>
                                        
                                        @php
                                            $cnt = $cnt + 1;
                                        @endphp
                                    @endforeach

                                    {{-- @if (($cnt) % 6 !== 0) --}}
                                        </table> </div> </div>
                                    {{-- @endif --}}
                                </div>

                                @if ($all_display['property_features'] == 1 && !empty($property->property_features))
                                <div class="row" style="margin:0; margin-top:20px;">
                                        <div class="table-responsive">
                                            <table class="table table-bordered  table-hover">
                                                <tr class="donthover">
                                                    <td>
                                                        Property Features
                                                    </td>
                                                    <td>
                                                        @php
                                                            $ftrs  = $property->property_features;
                                                            $ftrs = explode(",", $ftrs);
                                                        @endphp

                                                        @foreach ($ftrs as $item)
                                                            @php
                                                                $item = trim($item);
                                                            @endphp
                                                            @if ( strlen($item) > 0 )
                                                                <a class="hoverme" style="text-decoration:none; color:black; display:inline-block;
                                                                    margin:3px; padding: 10px; background-color:#F7F7F7; border-radius:10px;" 
                                                                    href="{{ url('properties?features[]='.$item) }}">
                                                                    {{ $item }}
                                                                </a>
                                                            @endif
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                @endif
                        </div>
                    </div>

                    @if( !empty($property->property_additional_details) &&  $all_display['property_additional_details'] == 1)
                        <div class="card" style="margin:30px; border-radius: 0;">
                            <div class="card-header" style="color: {{ setting('site.color_accent') }};"><strong>Additional Details</strong></div>
                            <div class="card-body">
                                {!! $property->property_additional_details !!}
                            </div>
                        </div>
                    @endif

                    <style>
                        .navigationArea button{
                            color:	{{ setting('site.color_accent') }};
                            background-color:{{ setting('site.color_secondary') }};
                            border: none;
                            cursor:pointer;
                            padding: 5px;
                            font-weight:bold;
                        }

                        .navigationArea button#prv{
                            background-color:{{ setting('site.color_secondary') }};
                        }

                        .navigationArea button#nxt{
                            background-color:{{ setting('site.color_secondary') }};
                        }

                        .navigationArea button:hover{
                            opacity: .8;
                        }

                        .navigationArea button#nxt:disabled{
                            background-color: grey;
                            color: #eee;
                        }
                        .navigationArea button#prv:disabled{
                            background-color: grey;
                            color: #eee;
                        }
                        .navigationArea button#btsr:disabled{
                            background-color: grey;
                            color: #eee;
                        }

                        .navigationArea button:disabled:hover{
                            opacity: 1;
                        }
                    </style>

                    <div class="navigationArea" style="margin:30px; text-align:right;">
                        <button id="prv" disabled="disabled" style="" title="Go to the previous property from the last search results">
                            Prev
                        </button>
                        <button id="nxt" disabled="disabled" title="Go to the next property from the last search results" >
                            Next
                        </button>
                        <button id="btsr" disabled="disabled" title="See the last search results again!">
                            Back to Search Results
                        </button>
                    </div>
                    {{-- set the links via js --}}
                    <script>
                        var ls = localStorage.getItem('searched');
                        if(ls){
                            ls = JSON.parse(ls);
                            // set the search result button
                            // console.log(ls);
                            document.getElementById('btsr').disabled = false;
                            document.getElementById('btsr').setAttribute('onclick','window.location.href = "' +  ls.search_url + '"');
                            // for next & prev send ajax req
                            // make url
                            var url = ls.search_url.split('?');
                            var rightpart = "";
                            if(url.length >= 2){
                                rightpart = url[1];
                            }
                            var xmlhttp = new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function(){
                                if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                                    var data = xmlhttp.responseText;
                                    // console.log(data);
                                    data = JSON.parse(data);
                                    // console.log(id, data);
                                    // now find the current id
                                    var prv = document.getElementById('prv');
                                    var nxt = document.getElementById('nxt');
                                    for(var i=0; i < data.length; ++i){
                                        if(data.length == 1) break;
                                        if(id == data[i]){
                                            if(i == 0){
                                                nxt.disabled = false;
                                                nxt.setAttribute('onclick', 'window.location.href = "/properties/' + data[i+1] + '"');
                                            }
                                            else if( i == data.length - 1){
                                                prv.disabled = false;
                                                prv.setAttribute('onclick', 'window.location.href = "/properties/' + data[i-1] + '"');
                                            }
                                            else{
                                                prv.disabled = false;
                                                nxt.disabled = false;
                                                prv.setAttribute('onclick', 'window.location.href = "/properties/' + data[i-1] + '"');
                                                nxt.setAttribute('onclick', 'window.location.href = "/properties/' + data[i+1] + '"');
                                            }
                                        }
                                    }
                                }
                            }
                            xmlhttp.open("GET", '/properties/getNavigation?' + rightpart, true);
                            xmlhttp.send();
                        }
                    </script>

                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="card" style="margin: 30px;">
                    <div class="card-header" style="color: {{ setting('site.color_accent') }};"><strong>Share This</strong></div>
                    <div class="card-body">
                            <div class="addthis_inline_share_toolbox" ></div>
                    </div>
                </div>

                <style>
                    /* textarea::placeholder{
                        color: black !important;
                    } */
                </style>
                
                <div class="row" style="margin:0px; border-radius: 0;">
                    <div class="col-sm-6">
                            <div class="card" >
                                <div class="card-header" style="color: {{ setting('site.color_accent') }};"><strong>Contact Us</strong></div>
                                    @php
                                        $phones = explode(",",$contact->office_phones);
                                        $mobiles = explode(",",$contact->mobile_numbers);
                                        $emails = explode(",",$contact->email_to_show);
                                    @endphp
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <tr>
                                                    <td>
                                                        Office
                                                    </td>
                                                    <td>
                                                        @php
                                                            $mult = 0;
                                                        @endphp
                                                        @foreach ($phones as $item)
                                                            @if ($mult > 0)
                                                                &nbsp;
                                                            @endif
                                                            <a class="hoverme" style="text-decoration:none; color:black; display:inline-block;" 
                                                                title="Call us"
                                                                href="{{ "tel:".$item }}">
                                                                {{ $item }}
                                                            </a>
                                                            @php
                                                                $mult += 1;
                                                            @endphp
                                                        @endforeach
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Mobile
                                                    </td>
                                                    <td>
                                                        @php
                                                            $mult = 0;
                                                        @endphp
                                                        @foreach ($mobiles as $item)
                                                            @if ($mult > 0)
                                                                &nbsp;
                                                            @endif
                                                            <a class="hoverme" style="text-decoration:none; color:black; display:inline-block;" 
                                                                title="Call us"
                                                                href="{{ "tel:".$item }}">
                                                                {{ $item }}
                                                            </a>
                                                            @php
                                                                $mult += 1;
                                                            @endphp
                                                        @endforeach
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Email
                                                    </td>
                                                    <td>
                                                        @php
                                                            $mult = 0;
                                                        @endphp
                                                        @foreach ($emails as $item)
                                                            @if ($mult > 0)
                                                                &nbsp;
                                                            @endif
                                                            <a class="hoverme" style="text-decoration:none; color:black; display:inline-block;" 
                                                                title="Email us"
                                                                href="{{ "mailto:".$item }}">
                                                                {{ $item }}
                                                            </a>
                                                            @php
                                                                $mult += 1;
                                                            @endphp
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                    </div>
                    <div class="col-sm-6">
                            <div class="card">
                                <div class="card-header" style="color: {{ setting('site.color_accent') }};"><strong>Message Us</strong></div>
                                <div class="card-body">
                                        {{-- TODO :: implement it. Mail send regarding this property with message --}}
                                        <form action="sendmail" method="post">
                                            <input style="border-bottom: 0 !important;" class="form-control" type="text" name="name" placeholder="Name" required>
                                            <input style="border-bottom: 0 !important;" class="form-control" type="email" name="email" placeholder="Email" required>
                                            <input style="border-bottom: 0 !important;" class="form-control" type="phone" name="phone" placeholder="Phone Number" required>
                                            <textarea class="form-control" type="textarea" name="message" placeholder="Your Message" required></textarea>
                                            <button type="submit" title="Send Your Message / Comments"
                                                style="background-color:{{ setting('site.color_accent') }}; border:none; color:{{ setting('site.color_secondary') }}; font-weight:bold;"
                                                class="btn btn-primary submit-button">Send Message</button>
                                        </form>
                                </div>
                            </div>
                    </div>
                        
                        
                </div>
            
                    
                    
                </div>

            </div>
            <div class="col-sm-3 for_mobile_1">
                
                @if($property->property_video !== null && $all_display['property_video'] == 1)
                    @php
                        $video = $property->property_video;
                        $display = false;
                        if(strpos($video, 'src="')){
                            $pos = strpos($video, 'src="');
                            $rest = substr($video, $pos+5);
                            $pos2 = strpos($rest, '"');
                            $display = substr($rest, 0, $pos2);
                            // echo $pos."!!!".$rest."!!!".$pos2."!!!".$display;
                        }
                    @endphp
                    @if($display)
                        <div class="card" style="margin-top:0;">
                            <div class="card-header" ><strong>Property Video</strong></div>
                            <div class="card-body no-padding">
                                <iframe style="width: 100%;"
                                    src="{{ $display }}?rel=0" 
                                    frameborder="0" 
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                                </iframe>
                            </div>
                        </div>
                    @endif
                @endif
                
                @if($property->property_map !== null && $all_display['property_map'] == 1)
                    @php
                        $video = $property->property_map;
                        $display = false;
                        if(strpos($video, 'src="')){
                            $pos = strpos($video, 'src="');
                            $rest = substr($video, $pos+5);
                            $pos2 = strpos($rest, '"');
                            $display = substr($rest, 0, $pos2);
                            // echo $pos."!!!".$rest."!!!".$pos2."!!!".$display;
                        }
                    @endphp

                    @if($display)
                        <div class="card" 
                        @if ($property->property_video == null )
                            style="margin-top:0;"
                        @endif
                        >
                            <div class="card-header"><strong>Property in Google Map</strong></div>
                            <div class="card-body no-padding">
                                    <iframe 
                                        src="{{ $display }}" frameborder="0" 
                                        style="border:0; width:100%;" 
                                        allowfullscreen>
                                    </iframe>
                            </div>
                        </div>
                    @endif
                @endif

                <div class="card"
                @if ($property->property_video == null && $property->property_map == null)
                            style="margin-top:0;"
                        @endif
                >
                    <div class="card-header"><strong>Recently Viewed Properties</strong></div>
                    <div class="card-body recent" style="padding:0;">
                        <ul class="recentx" id="recently_viewed">
                            
                        </ul>
                        
                    </div>

                    <script>
                        function predicateBy(prop){
                            return function(a,b){
                                if( a[prop] > b[prop]){
                                    return 1;
                                }else if( a[prop] < b[prop] ){
                                    return -1;
                                }
                                return 0;
                            }
                        }

                        var curr_id = "{!! $property->property_id !!}";
                        var recents = localStorage.getItem('recently_viewed');

                        if(recents){
                            recents = JSON.parse(recents);
                            recents.sort( predicateBy("time") );

                            var mainul = document.getElementById('recently_viewed');

                            var cnt = 0;

                            var visited = [];

                            for(var i=recents.length-1; i >= 0; --i){

                                if(cnt == 4) break;

                                if(curr_id == recents[i].id) continue;

                                if(visited.includes(recents[i].id)) continue;

                                var li = document.createElement('li');

                                var div1 = document.createElement('div');
                                div1.className = "pimage_side";

                                var a1 = document.createElement('a');
                                a1.setAttribute('href',recents[i].link);

                                var img = document.createElement('img');
                                img.setAttribute('src',recents[i].image_link);
                                img.setAttribute('width','120');
                                img.setAttribute('alt',recents[i].title);

                                a1.appendChild(img);
                                div1.appendChild(a1);

                                var div2 = document.createElement('div');
                                div2.className = "ptitle_side";

                                var a2 = document.createElement('a');
                                a2.setAttribute('href',recents[i].link);
                                a2.innerHTML = recents[i].title;

                                div2.appendChild(a2);

                                li.appendChild(div1);
                                li.appendChild(div2);

                                mainul.appendChild(li);

                                cnt++;
                                visited.push(recents[i].id);
                            }
                        }
                    </script>
                </div>
    
            </div>
        </div>

        @if (count($similar) > 0)
        <div class="similar_properties for_mobile_1">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- 4 properties here -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Similar Properties</h4>
                                <div class="row">
                                    @php
                                        $turn = 0;
                                    @endphp
                                    @foreach ($similar as $sim)
                                        @php
                                            $images = json_decode($sim->property_images);
                                            if(!$images || count($images) == 0){
                                                $images[] = "default_image.jpg";
                                            }
                                            $friendly_url = is_null($sim->slug) ? $sim->id : $sim->slug;
                                            $turn = $turn + 1;
                                        @endphp
                                        <div class="col-sm-2 for_mobile2" 
                                            @if ($turn % 2 == 0)
                                                style="box-shadow: 1px 2px 20px 10px #ccc;"
                                            @endif
                                            
                                            >
                                            <div class="similar_items">
                                                <div class="title_x">
                                                    <div class="property_city">
                                                        <a href="{{ url('properties/'.$friendly_url) }}">{{ $sim->property_city }}</a>
                                                    </div>
                                                </div>
                                                <figure>
                                                    <a href="{{ url('properties/'.$friendly_url) }}">
                                                        <img src="{{ asset('storage/'.$images[0]) }}" alt="">
                                                    </a>
                                                </figure>
                                                
                                                <!-- We display the price here -->
                                                <span id="price">&euro;{{ number_format($sim->property_price, 2) }} - For {{ $sim->property_type}}</span>
                                                <h4>
                                                    <a href="{{ url('properties/'.$friendly_url) }}">
                                                            {{ $sim->property_title }}
                                                    </a>
                                                </h4>
                                                <p>{{ html_entity_decode(strip_tags(mb_substr($sim->property_description,0,70))) }}...</p>
                                                <div class="similar_items_title">
                                                    <div class="more_detail">
                                                        <a class="more-details-x" href="">
                                                            More Details <i class="fa fa-caret-right"></i>
                                                        </a>
                                                    </div>
                                                    <div class="prop_id" style="margin-top:-9px;">
                                                            <h6>{{ $sim->property_id }}</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
       
    </div>

    

    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
@endsection