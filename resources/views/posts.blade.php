@extends('layouts.app')

@section('content')


<div class="container">
    <style>
        
    </style>
    <h2 style="margin-bottom:30px;margin-top:30px;">News</h2>
    <div class="row">
        
    @foreach($posts as $post)
        <div class="col-md-3">
            <a class="donthoverme"href="/news/{{ $post->slug }}" style="text-decoration:none;">
                <img src="{{ Voyager::image( $post->image ) }}" 
                style="max-width:100%; width:300px; height:200px;">
                <span>{{ $post->title }}</span>
            </a>
        </div>
    @endforeach
    </div>
    
</div>
@endsection