@extends('layouts.app')

@section('content')
    <style>
        .py-4 {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
        }
    </style>
    @include('slider')


    @include('layouts.search')

    @include('featured')

    @include('layouts.separator')

    @include('promotion')

    @include('layouts.separator')

    @include('mostrecent')

    @include('layouts.separator')

    @include('latest_news')

@endsection