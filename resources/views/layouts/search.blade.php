
<link rel="stylesheet" href="{{ asset('css/searchbox.css') }}">

<style>
    .make-it-float {
        position: absolute !important;
        top: 30%;
        left: 19%;
        z-index: 2;
    }

    .show-more-features{
        color:#FFD700;
    }

    .make-it-float label{
        color: {{ setting('site.color_accent') }};
        font-weight: bold;
    }

    /* temp style */
    select, input{
        color : {{ setting('site.color_accent') }} !important;
        background-color: {{ setting('site.color_secondary') }} !important;
    }
    input::placeholder{
        color: {{ setting('site.color_accent') }};
        opacity: 1;
    }
    select option{
        color: {{ setting('site.color_accent') }};
        background-color: {{ setting('site.color_secondary') }};
    }
    /* temp style ends */

    @media (max-width : 3200px){
        .show-more-features{
            color:#FFD700;
        }
    }

    @media (max-width : 1700px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 16%;
            padding: 20px;
        }
        .show-more-features{
            color:#FFD700;;
        }
    }
    
    @media (max-width : 1500px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 14%;
        }
        
    }
    
    @media (max-width : 1400px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 7%;
        }
        #search_form{
            max-width: 985px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1360px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 7%;
        }
        #search_form{
            max-width: 985px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1330px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 6.5%;
        }
        #search_form{
            max-width: 985px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1310px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 6%;
        }
        #search_form{
            max-width: 985px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1280px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 5.5%;
        }
        #search_form{
            max-width: 985 px;
            margin: 0 auto;
        }
    }
    @media (max-width : 1250px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 4%;
        }
        #search_form{
            max-width: 985px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1230px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 3%;
        }
        #search_form{
            max-width: 985px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1210px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 2%;
        }
        #search_form{
            max-width: 985px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1199px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 11%;
        }
        #search_form{
            max-width: 750px;
            margin: 0 auto;
        }
    }
    @media (max-width : 1170px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 9.5%;
        }
        #search_form{
            max-width: 750px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1130px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 8.5%;
        }
        #search_form{
            max-width: 750px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1100px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 7.5%;
        }
        #search_form{
            max-width: 750px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1080px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 6.5%;
        }
        #search_form{
            max-width: 750px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1060px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 5.5%;
        }
        #search_form{
            max-width: 750px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1050px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
        }
        #search_form{
            max-width: 750px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1040px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 5%;
        }
        #search_form{
            max-width: 760px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 1010px){
        .make-it-float {
            position: absolute !important;
            top: 30%;
            left: 4%;
        }
        #search_form{
            max-width: 750px;
            margin: 0 auto;
        }
    }
    
    @media (max-width : 991px){
        .make-it-float {
            position: relative!important;
            padding: 0;
            left: 0;
            top: 0;
        }
        .show-more-features{
            color:black;
        }
        .search-option label{
            color: black;
        }
        #search_form{
            background: transparent !important;
            padding: 10px !important;
        }
    }
    @media (max-width : 571px){
        .show-more-features{
            color:black;
        }
        /* .search-option{
            display: block;
        } */
    }

    .make-it-float{
        background-color: transparent !important;
        border: none;
        box-shadow: none;
    }

    
</style>

<div class="container contents make-it-float" style="margin-top:30px;">
    {{-- <div class="row"> --}}
            {{-- box-shadow: 0 2px 5px 0 rgba(0,0,0,.4); --}}
        {{-- <div class="card" style="margin:0;  background-color:transparent; border:none;"> --}}
        {{-- <div class="card-header">
                <h3 class="search-heading">
                        <i class="fa fa-search"></i>
                        <i class="fa fa-home fa-lg"></i>
                    </h3>
        </div> --}}
        {{-- <div class="card-body" > --}}
         <form id="search_form" class="form" action="{{ URL::to('properties') }}" method="get" 
         style="padding:15px;background:rgba(0,0,0,0.25);">
                        @php
                            $total = 0;
                        @endphp
                        <div class="search-option">
                            <label for="property_type">Property Type</label>
                            <select name="property_type" id="property_type">
                                @foreach ($types as $type)
                                    <option value="{{ $type }}" 
                                    <?php echo (isset($_GET['property_type']) && $_GET['property_type'] == $type )? 'selected' : ''; ?> >
                                        {{ $type }}
                                    </option>
                                    @php
                                        $total += $frq[$type];
                                    @endphp
                                @endforeach
                                @if (isset($_GET['property_type']) && $_GET['property_type'] != 'Any')
                                    <option value="Any">Any</option>
                                @else
                                    <option value="Any" selected>Any</option>
                                @endif
                            </select>
                        </div>
                        @php
                            $total = 0;
                        @endphp
                        <div class="search-option">
                            <label for="property_status">Property Status</label>
                            <select name="property_status" id="property_status">
                                @foreach ($statuses as $status)
                                    <option value="{{ $status }}" 
                                    <?php echo (isset($_GET['property_status']) && $_GET['property_status'] == $status )? 'selected' : ''; ?> >
                                        {{ $status }}
                                    </option>
                                    @php
                                        $total += $frq[$status];
                                    @endphp
                                @endforeach
                                @if (isset($_GET['property_status']) && $_GET['property_status'] != 'Any')
                                    <option value="Any">Any</option>
                                @else
                                    <option value="Any" selected>Any</option>
                                @endif
                            </select>
                        </div>
        
                        @php
                            $total = 0;
                        @endphp
                        <div class="search-option">
                            <label for="property_city">Property City</label>
                            <select name="property_city" id="property_city">
                                @foreach ($cities as $city)
                                    <option value="{{ $city }}" 
                                    <?php echo (isset($_GET['property_city']) && $_GET['property_city'] == $city )? 'selected' : ''; ?> >
                                        {{ $city }}
                                    </option>
                                    @php
                                        $total += $frq[$city];
                                    @endphp
                                @endforeach
                                @if (isset($_GET['property_city']) && $_GET['property_city'] != 'Any')
                                    <option value="Any">Any</option>
                                @else
                                    <option value="Any" selected>Any</option>
                                @endif
                            </select>
                        </div>
        
                        @php
                            $total = 0;
                        @endphp
                        <div class="search-option">
                            <label for="property_location">Property Location</label>
                            <select name="property_location" id="property_location">
                                @foreach ($loc as $lc)
                                    <option value="{{ $lc }}" 
                                    <?php echo (isset($_GET['property_location']) && $_GET['property_location'] == $lc )? 'selected' : ''; ?> >
                                        {{ $lc }}
                                    </option>
                                    @php
                                        $total += $frq[$lc];
                                    @endphp
                                @endforeach
                                @if (isset($_GET['property_location']) && $_GET['property_location'] != 'Any')
                                    <option value="Any">Any</option>
                                @else
                                    <option value="Any" selected>Any</option>
                                @endif
                            </select>
                        </div>
        
                        @php
                            $total = 0;
                        @endphp
                        <div class="search-option">
                            <label for="property_bedrooms">Min Beds</label>
                            <select name="property_bedrooms" id="property_bedrooms">
                                <option value="Studio" 
                                    <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == 'Studio' )? 'selected' : ''; ?>
                                >Studio</option>
                                <option value="1" 
                                    <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == '1' )? 'selected' : ''; ?>
                                >1</option>
                                <option value="2" 
                                    <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == '2' )? 'selected' : ''; ?>
                                >2</option>
                                <option value="3" 
                                    <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == '3' )? 'selected' : ''; ?>
                                >3</option>
                                <option value="4" 
                                    <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == '4' )? 'selected' : ''; ?>
                                >4</option>
                                <option value="4+" 
                                    <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == '4+' )? 'selected' : ''; ?>
                                >4+</option>
                                <option value="Any"
                                    <?php echo ((isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == 'Any' ) || !isset($_GET['property_bedrooms']) )? 'selected' : ''; ?>
                                >Any</option>
                            </select>
                        </div>
        
                        @php
                            $total = 0;
                        @endphp
                        <div class="search-option">
                            <label for="property_bathrooms">Min Baths</label>
                            <select name="property_bathrooms" id="property_bathrooms">
                                <option value="1" 
                                    <?php echo (isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == '1' )? 'selected' : ''; ?>
                                >1</option>
                                <option value="2" 
                                    <?php echo (isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == '2' )? 'selected' : ''; ?>
                                >2</option>
                                <option value="3" 
                                    <?php echo (isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == '3' )? 'selected' : ''; ?>
                                >3</option>
                                <option value="4" 
                                    <?php echo (isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == '4' )? 'selected' : ''; ?>
                                >4</option>
                                <option value="4+" 
                                    <?php echo (isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == '4+' )? 'selected' : ''; ?>
                                >4+</option>
                                <option value="Any"
                                    <?php echo ((isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == 'Any' ) || !isset($_GET['property_bedrooms']) )? 'selected' : ''; ?>
                                >Any</option>
                            </select>
                        </div>
        
                        @php
                            $total = 0;
                        @endphp
                        <div class="search-option">
                            <label for="property_price">Property Price</label>
                            <select name="property_price" id="property_price">
                                <option value="UP TO 100K" 
                                    <?php echo (isset($_GET['property_price']) && $_GET['property_price'] == 'UP TO 100K' )? 'selected' : ''; ?>
                                >UP TO 100K</option>
                                <option value="UP TO 200K" 
                                    <?php echo (isset($_GET['property_price']) && $_GET['property_price'] == 'UP TO 200K' )? 'selected' : ''; ?>
                                >UP TO 200K</option>
                                <option value="UP TO 300K" 
                                    <?php echo (isset($_GET['property_price']) && $_GET['property_price'] == 'UP TO 300K' )? 'selected' : ''; ?>
                                >UP TO 300K</option>
                                <option value="UP TO 400K" 
                                    <?php echo (isset($_GET['property_price']) && $_GET['property_price'] == 'UP TO 400K' )? 'selected' : ''; ?>
                                >UP TO 400K</option>
                                <option value="400K+" 
                                    <?php echo (isset($_GET['property_price']) && $_GET['property_price'] == '400K+' )? 'selected' : ''; ?>
                                >400K+</option>
                                <option value="Any"
                                    <?php echo ((isset($_GET['property_price']) && $_GET['property_price'] == 'Any' ) || !isset($_GET['property_price']) )? 'selected' : ''; ?>
                                >Any</option>
                            </select>
                        </div>
        
                        <div class="search-option">
                                <label for="min-area">Min Area <span>(Sq m)</span></label>
                                <input 
                                    type="text" 
                                    name="min-area" 
                                    id="min-area"
                                    value="<?php echo isset($_GET['min-area']) ? $_GET['min-area'] : '' ?>"
                                    pattern="[0-9]+"
                                    placeholder="Any" 
                                    title="Please only provide digits!">
                        </div>
        
                        <div class="search-option">
                            <label for="max-area">Max Area <span>(Sq m)</span></label>
                            <input 
                                type="text" 
                                name="max-area" 
                                id="max-area" 
                                value="<?php echo isset($_GET['max-area']) ? $_GET['max-area'] : '' ?>"
                                pattern="[0-9]+"
                                placeholder="Any" 
                                title="Please only provide digits!">
                        </div>
        
                        <div class="search-option">
                            <label for="keyword_search">Keyword</label>
                            <input type="text" name="keyword_search" id="keyword_search" value="<?php echo isset($_GET['keyword_search']) ? $_GET['keyword_search'] : '' ?>" 
                                placeholder="Any">
                        </div>
        
                        <div class="search-option">
                            <label for="property-id-txt">Property ID</label>
                            <input type="text" name="property_id" id="property-id-txt" 
                                value="<?php echo isset($_GET['property_id']) ? $_GET['property_id'] : '' ?>" placeholder="Any" autocomplete="off">
                        </div>
        
                        <div class="search-below search-button">
                            <input name="search" type="submit" id="sbtn" value="Search" class="btn btn-clr" disabled="disabled">
                            <input type="button" value="Reset" class="btn btn-clr" onclick=" if(confirm('Are you sure?')) window.location.href = '/properties';">
                        </div>
        
                        <div class="show-more-features"  onclick="toggleFeatures();">
                            <i class="fa fa-search-plus"></i>
                            Search for specific features
                        </div>
                        <div class="hide collapsed" id="specific-features" style="display:none; max-width: 947px; ">
                            @php
                                $fn = 0;
                            @endphp
                            @foreach ($features as $feature)
                            @php
                                $checked = "";
                                if( isset($_GET['features']) ){
                                    $ftt = $_GET['features'];
                                    foreach ($ftt as $ftrv) {
                                        if($feature == $ftrv){
                                            $checked = "checked";
                                            break;
                                        }
                                    }
                                }
                            @endphp
                                <div class="option-bar" style="margin:10px 0 0 0; font-size:14px;">
                                        <input type="checkbox" id="{{ $fn }}" name="features[]" value="{{ $feature }}" {{ $checked }}>
                                    <label for="{{ $fn }}">{{ $feature }} <small>({{ $frq[$feature] }})</small></label>
                                </div>
                                @php
                                    $fn += 1;
                                @endphp
                            @endforeach
                        </div>
                    </form>
        {{-- </div> --}}
        {{-- </div> --}}
    {{-- </div> --}}
</div>