@foreach($items as $menu_item)
        <option value="{{ $menu_item->url }}">{{ $menu_item->title }}</option>
        @foreach ($menu_item->children as $child)
                <option value="{{ $child->url }}"> - {{ $child->title }}</option>
        @endforeach
@endforeach