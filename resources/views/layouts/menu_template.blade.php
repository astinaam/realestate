@foreach($items as $menu_item)
        @php
                $cnt = count($menu_item->children);
        @endphp
        <li>
                <a href="{{ $menu_item->url }}">{{ $menu_item->title }}</a>
                
                @if ($cnt > 0)
                <ul class="sub-menu" style="display: none;">
                @foreach ($menu_item->children as $child)
                        
                <li>
                        <a href="{{ $child->url }}">{{ $child->title }}</a>
                </li>
                        
                @endforeach
                </ul>
                @endif
        </li>
@endforeach