<link rel="stylesheet" href="{{ asset('css/footer_css.css') }}" />

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="footer_post">
                    <h1>Contact Info</h1>

                    <aside id="nav_menu-2" class="widget widget_nav_menu">
                        <p>
                                <strong>Address : </strong>   S.P Business Center, 17 Neofytou Nikolaide & Kilkis str,Paphos. 3rd Flr, Office 301
                        </p>
                        <p>
                                <strong>Tel : </strong> <a href="tel:00357 7000 88 20 ">00357 7000 88 20 </a>
                        </p>
                        <p>
                                <strong>Email : </strong> <a href="mailto:info@kpphomes.com">info@kpphomes.com</a>
                        </p>
                    </aside>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="footer_post">
                    <h1>Useful Links</h1>

                    <aside  class="widget widget_nav_menu">
                        <div>
                            <ul class="menu">
                                <li
                                >
                                    <a
                                        href="{{ URL::to('aboutus') }}"
                                        >About Us</a
                                    >
                                </li>
                                <li
                                >
                                    <a
                                        href="{{ URL::to('contact') }}"
                                        >Contact Us</a
                                    >
                                </li>
                                <li
                                >
                                    <a
                                        href="{{ URL::to('aboutcyprus') }}"
                                        >About Cyprus</a
                                    >
                                </li>
                                <li
                                >
                                    <a
                                        href="{{ URL::to('whycyprus') }}"
                                        >Why Cyprus</a
                                    >
                                </li>
                                
                                
                                
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="footer_post">
                    <h1>Services</h1>

                    <aside class="widget widget_nav_menu">
                        <div>
                            <ul class="menu">
                                <li
                                >
                                    <a
                                        href="{{ URL::to('immigration-services') }}"
                                        >Immigration Services</a
                                    >
                                </li>
                                <li
                                >
                                    <a
                                        href="{{ URL::to('immigration-services') }}"
                                        >Interior Design</a
                                    >
                                </li>
                            </ul>
                        </div>
                    </aside>

                    {{-- <h1>Immigration Services</h1>

                    <aside id="nav_menu-5" class="widget widget_nav_menu">
                        <div class="menu-immigration-services-container">
                            <ul id="menu-immigration-services" class="menu">
                                <li
                                    id="menu-item-257"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-257"
                                >
                                    <a
                                        href="http://www.kppcorporate.com/permanent-citizenship/"
                                        >Permanent Citizenship</a
                                    >
                                </li>
                                <li
                                    id="menu-item-258"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-258"
                                >
                                    <a
                                        href="http://www.kppcorporate.com/cyprus-passport/"
                                        >Cyprus Passport</a
                                    >
                                </li>
                            </ul>
                        </div>
                    </aside> --}}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer_bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="pull-left copyright">
                    <aside id="text-7" class="widget widget_text">
                        <div class="textwidget">
                            <p>
                                © <?php echo date("Y"); ?> All rights reserved.
                                <a
                                    href=" {{ URL::to('privacy-policy') }}"
                                    >Privacy Policy</a
                                >
                                I
                                <a
                                    href="{{ URL::to('terms-of-use') }}"
                                    >Terms of Use</a
                                >
                            </p>
                        </div>
                    </aside>
                </div>

                {{-- <div class="pull-right footer_social">
                    <aside id="text-8" class="widget widget_text">
                        <div class="textwidget">
                            <ul>
                                <li>
                                    <a
                                        target="_blank"
                                        href="https://www.facebook.com/kppcorporateservices/"
                                        ><i class="fa fa-facebook"></i
                                    ></a>
                                </li>
                                <li>
                                    <a
                                        target="_blank"
                                        href="https://www.linkedin.com/company-beta/15231935/"
                                        ><i class="fa fa-linkedin-square"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                </div> --}}
            </div>
        </div>
    </div>
</div>
