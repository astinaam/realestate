<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- addressbar icon - from voyager panel setting --}}
    <link rel="icon" type="image/png" href="{{ asset('storage/'.setting('site.web_icon')) }}"/>

    {{-- make title dynamic --}}
    @php
        $title = 'Home';
    @endphp
    @if (\Request::is('properties'))
        @php
            $title = 'Search';
        @endphp
    @endif

    @if (\Request::is('properties/*'))
        @php
            $title = $property->property_title;
        @endphp
    @endif
    
    @if (\Request::is('about'))
        @php
            $title = 'About Us';
        @endphp
    @endif

    @if (\Request::is('news'))
        @php
            $title = 'News';
        @endphp
    @endif
    
    @if (\Request::is('whycyprus'))
        @php
            $title = 'Why Cyprus';
        @endphp
    @endif

    <title>{{ $title }} - {{ setting('site.title') }} - {{ setting('site.description') }}</title>

    <!-- Scripts -->
    
    <script src="{{ asset('js/search.js') }}" defer></script>

    {{-- add jquery --}}
    {{-- @if (\Request::is('properties')) --}}
        <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    {{-- @endif --}}
    <!-- Fonts -->
    
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    {{-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"> --}}

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> --}}
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <script>
    function setItems(){
        var the_link = window.location.href;

        var retrieve = localStorage.getItem('recently_viewed');
        
        var container = [];

        var dt = new Date();
        
        if(retrieve){
            container = JSON.parse(retrieve);
            // console.log(retrieve);
            container.push({ id : the_id, title : the_title, link : the_link, image_link : the_image_link, time : dt});
            localStorage.setItem('recently_viewed', JSON.stringify(container));
        }
        else{
            container.push({ id : the_id, title : the_title, link : the_link, image_link : the_image_link, time : dt});
            localStorage.setItem('recently_viewed', JSON.stringify(container));
        }
    }
    </script>

    <style>
        #scroll-top {
            display: none;
            position: fixed;
            right: 30px;
            bottom: 40px;
            height: 42px;
            width: 42px;
            line-height: 42px;
            text-align: center;
            color: {{ setting('site.color_accent') }};
            background-color: {{ setting('site.color_secondary') }};
            z-index: 9999;
            -webkit-transition: all 0.25s ease-in-out;
            transition: all 0.25s ease-in-out;
        }

        @media (max-width:746px){
            #scroll-top{
                display: none !important;
            }
        }

        .card-header{
            background-color: #3960a2;
            color: white;
        }

        .btn-clr{
            background-color: black !important;
            color: #FFD700;
            font-weight: bold;
        }

        .page-item.active .page-link{
            color: {{ setting('site.color_accent') }};
            background-color: {{ setting('site.color_secondary') }};
            border-color: {{ setting('site.color_secondary') }};
        }
    </style>
</head>
<body @if (\Request::is('properties/*')) onload="setItems();" @endif>
    <div id="app">
        
        @include('menu')

        <main class="py-4" style="padding-top:0 !important;">
            @yield('content')
        </main>

        @include('layouts.footer')
    </div>
    
    
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" ></script> --}}

    <script src="{{ asset('js/patches.js') }}" defer></script>

    {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> --}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> --}}
    {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> --}}


    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bf788bcff7b9be5"></script>

    <a href="#top" id="scroll-top"><i class="fa fa-chevron-up"></i></a>

    <script>
        // back to top

        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("scroll-top").style.display = "block";
            } else {
                document.getElementById("scroll-top").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }


    </script>

</body>
</html>
