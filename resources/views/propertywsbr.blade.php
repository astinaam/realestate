@extends('layouts.app') 
@section('content')

<!-- here parameters -->

<style>
    .card-header{
        background-color: black;
    }
    .search-option label, .show-more-features{
        color : {{ setting('site.color_secondary') }};
        font-weight: bold;
    }
    select, input{
        color : {{ setting('site.color_accent') }} !important;
        background-color: {{ setting('site.color_secondary') }} !important;
    }
    input::placeholder{
        color: {{ setting('site.color_accent') }};
        opacity: 1;
    }
    select option{
        color: {{ setting('site.color_accent') }};
        background-color: {{ setting('site.color_secondary') }};
    }
    .option-bar label{
        color : {{ setting('site.color_accent') }};
    }
</style>

<link rel="stylesheet" href="{{ asset('css/searchbox.css') }}">

<div class="conatiner">
    <div class="row">
        <div class="col-sm-9">
                <div class="container contents in-sidebar" style="margin-top:30px;">
                        <div class="sort-controls" style="text-align:right;">
                                <strong>Sort By:</strong>
                                &nbsp;
                                <select name="sort-properties" id="sort-properties" onchange="sortby();" data-href="{{ URL::to('properties') }}">
                                        <option value="default" <?php if(isset($_GET['sortby']) && $_GET['sortby'] == 'default') echo 'selected="selected"'; ?> >Default Order</option>
                                        <option value="price-asc" <?php if(isset($_GET['sortby']) && $_GET['sortby'] == 'price-asc') echo 'selected="selected"'; ?>  >Price Low to High</option>
                                        <option value="price-desc" <?php if(isset($_GET['sortby']) && $_GET['sortby'] == 'price-desc') echo 'selected="selected"'; ?> >Price High to Low</option>
                                        <option value="date-asc" <?php if(isset($_GET['sortby']) && $_GET['sortby'] == 'date-asc') echo 'selected="selected"'; ?> >Date Old to New</option>
                                        <option value="date-desc" <?php if(isset($_GET['sortby']) && $_GET['sortby'] == 'date-desc') echo 'selected="selected"'; ?> >Date New to Old</option>
                                </select>
                        </div>
                        
                        <div class="row" style="margin-top:00px;">
                    
                            @if (count($properties) == 0)
                                <h4 style="text-align:center;    width: 180px;
                                margin: 0 auto;"> No Properties Found!</h4>
                            @endif
                            
                            @foreach ($properties as $property)
                            @php
                                $images = json_decode($property->property_images);
                                if(!$images || count($images) == 0){
                                    $images[] = "default_image.jpg";
                                }
                    
                                $friendly_url = is_null($property->slug) ? $property->id : $property->slug;
                            @endphp
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>
                                            <a href="{{ url('properties/'.$friendly_url) }}" style="color:white;">
                                                {{ $property->property_title }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="card-body" style="padding: 5px !important;">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                    <figure>
                                                        {{-- <a href="{!! route('properties.show', [$property->id])  !!}">     --}}
                                                        <a href="{{ url('properties/'.$friendly_url) }}">    
                                                            <img width="244" height="163" 
                                                                src="{{ asset('storage/'.$images[0]) }}" 
                                                                class="" 
                                                                alt="{{ $property->property_title }}" >
                                                        </a>
                                                    <figcaption class="resale">{{ $property->property_status }}</figcaption>
                                                    </figure>
                                            </div>
                                            <div class="col-sm-6">
                                                    <div class="detail">
                                                        <h5 class="price">
                                                            &euro;{{ number_format($property->property_price,2) }} <small> -  {{ $property->property_type }}</small>            
                                                        </h5>
                                                        <p> {{ mb_substr(html_entity_decode(strip_tags($property->property_description)),0,50) }} ...</p>
                                                        <div class="more-details">
                                                            <div id="more-detail" class="more-details-1">
                                                                <a class="" 
                                                                    href="{{ url('properties/'.$friendly_url) }}">More Details <i class="fa fa-caret-right"></i>
                                                                </a>
                                                            </div>
                                                            <div id="prop_id" class="more-details-2">
                                                                <h6>{{ $property->property_id }}</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer" style="padding:6px;">
                                        <div class="property-meta">
                    
                                            @if ($property->property_bedrooms)
                                                <span><i class="fa fa-bed"></i> {{$property->property_bedrooms}}&nbsp;Bedrooms</span>
                                            @endif
                                            
                                            @if ($property->property_bathrooms)
                                            <span><i class="fas fa-bath"></i> {{$property->property_bathrooms}}&nbsp;Bathrooms</span>
                                            @endif
                    
                                            @php
                                                $date = date(strtotime($property->created_at));
                                                $date = getdate($date);
                                                $month = $date["month"];
                                                $day = $date["mday"];
                                            @endphp
                                            
                                            @if ($property->created_at)
                                            <span><i class="fa fa-calendar"></i>&nbsp;{{ $day." ".$month }}</span>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    
                        <div class="pagination">
                            {{ $properties->links() }}
                        </div>

                        @if ($properties->lastPage() == 1)
                            <style>
 
                                .property-per-page{
                                    margin-top:-45px !important; 
                                    margin-bottom: 0px !important;
                                }
                               
                            </style>
                        @endif

                        <div class="property-per-page" style="display:block; text-align:right; margin-top:-102px; margin-bottom:45px;">
                            <strong>Poperties per page:</strong>
                            &nbsp;
                            <select name="properties_per_page" id="properties_per_page" onchange="properties_per_page();" data-href="{{ URL::to('properties') }}" style="width:auto;">
                                    <option value="10" <?php if(isset($_GET['perpage']) && $_GET['perpage'] == 10) echo 'selected="selected"'; ?> >10</option>
                                    <option value="20" <?php if(isset($_GET['perpage']) && $_GET['perpage'] == 20) echo 'selected="selected"'; ?>  >20</option>
                                    <option value="50" <?php if(isset($_GET['perpage']) && $_GET['perpage'] == 50) echo 'selected="selected"'; ?> >50</option>
                                    <option value="100" <?php if(isset($_GET['perpage']) && $_GET['perpage'] == 100) echo 'selected="selected"'; ?> >100</option>
                            </select>
                        </div>

                    </div>
        </div>
        <style>
            @media (min-width: 576px){
                .col-sm-2 {
                    flex: 0 0 21.8%;
                    max-width: 22%;
                }
            }
        
        </style>
        <div class="col-sm-2 rightbar">
            <form id="search_form" class="form" action="{{ URL::to('properties') }}" method="post" style="padding:15px;">
                @php
                    $total = 0;
                @endphp
                <div class="search-option">
                    <label for="property_type">Property Type</label>
                    <select name="property_type" id="property_type">
                        @foreach ($types as $type)
                            <option value="{{ $type }}" 
                            <?php echo (isset($_GET['property_type']) && $_GET['property_type'] == $type )? 'selected' : ''; ?> >
                                {{ $type }}({{ $frq[$type] }})
                            </option>
                            @php
                                $total += $frq[$type];
                            @endphp
                        @endforeach
                        @if (isset($_GET['property_type']) && $_GET['property_type'] != 'Any')
                            <option value="Any">Any({{ $total }})</option>
                        @else
                            <option value="Any" selected>Any({{ $total }})</option>
                        @endif
                    </select>
                </div>
                @php
                    $total = 0;
                @endphp
                <div class="search-option">
                    <label for="property_status">Property Status</label>
                    <select name="property_status" id="property_status">
                        @foreach ($statuses as $status)
                            <option value="{{ $status }}" 
                            <?php echo (isset($_GET['property_status']) && $_GET['property_status'] == $status )? 'selected' : ''; ?> >
                                {{ $status }}({{ $frq[$status] }})
                            </option>
                            @php
                                $total += $frq[$status];
                            @endphp
                        @endforeach
                        @if (isset($_GET['property_status']) && $_GET['property_status'] != 'Any')
                            <option value="Any">Any({{ $total }})</option>
                        @else
                            <option value="Any" selected>Any({{ $total }})</option>
                        @endif
                    </select>
                </div>

                @php
                    $total = 0;
                @endphp
                <div class="search-option">
                    <label for="property_city">Property City</label>
                    <select name="property_city" id="property_city">
                        @foreach ($cities as $city)
                            <option value="{{ $city }}" 
                            <?php echo (isset($_GET['property_city']) && $_GET['property_city'] == $city )? 'selected' : ''; ?> >
                                {{ $city }}
                            </option>
                            @php
                                $total += $frq[$city];
                            @endphp
                        @endforeach
                        @if (isset($_GET['property_city']) && $_GET['property_city'] != 'Any')
                            <option value="Any">Any</option>
                        @else
                            <option value="Any" selected>Any</option>
                        @endif
                    </select>
                </div>

                @php
                    $total = 0;
                @endphp
                <div class="search-option">
                    <label for="property_location">Property Location</label>
                    <select name="property_location" id="property_location">
                        @foreach ($loc as $lc)
                            <option value="{{ $lc }}" 
                            <?php echo (isset($_GET['property_location']) && $_GET['property_location'] == $lc )? 'selected' : ''; ?> >
                                {{ $lc }}
                            </option>
                            @php
                                $total += $frq[$lc];
                            @endphp
                        @endforeach
                        @if (isset($_GET['property_location']) && $_GET['property_location'] != 'Any')
                            <option value="Any">Any</option>
                        @else
                            <option value="Any" selected>Any</option>
                        @endif
                    </select>
                </div>

                @php
                    $total = 0;
                @endphp
                <div class="search-option">
                    <label for="property_bedrooms">Min Beds</label>
                    <select name="property_bedrooms" id="property_bedrooms">
                        <option value="Studio" 
                            <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == 'Studio' )? 'selected' : ''; ?>
                        >Studio</option>
                        <option value="1" 
                            <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == '1' )? 'selected' : ''; ?>
                        >1</option>
                        <option value="2" 
                            <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == '2' )? 'selected' : ''; ?>
                        >2</option>
                        <option value="3" 
                            <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == '3' )? 'selected' : ''; ?>
                        >3</option>
                        <option value="4" 
                            <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == '4' )? 'selected' : ''; ?>
                        >4</option>
                        <option value="4+" 
                            <?php echo (isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == '4+' )? 'selected' : ''; ?>
                        >4+</option>
                        <option value="Any"
                            <?php echo ((isset($_GET['property_bedrooms']) && $_GET['property_bedrooms'] == 'Any' ) || !isset($_GET['property_bedrooms']) )? 'selected' : ''; ?>
                        >Any</option>
                    </select>
                </div>

                @php
                    $total = 0;
                @endphp
                <div class="search-option">
                    <label for="property_bathrooms">Min Baths</label>
                    <select name="property_bathrooms" id="property_bathrooms">
                        <option value="1" 
                            <?php echo (isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == '1' )? 'selected' : ''; ?>
                        >1</option>
                        <option value="2" 
                            <?php echo (isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == '2' )? 'selected' : ''; ?>
                        >2</option>
                        <option value="3" 
                            <?php echo (isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == '3' )? 'selected' : ''; ?>
                        >3</option>
                        <option value="4" 
                            <?php echo (isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == '4' )? 'selected' : ''; ?>
                        >4</option>
                        <option value="4+" 
                            <?php echo (isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == '4+' )? 'selected' : ''; ?>
                        >4+</option>
                        <option value="Any"
                            <?php echo ((isset($_GET['property_bathrooms']) && $_GET['property_bathrooms'] == 'Any' ) || !isset($_GET['property_bedrooms']) )? 'selected' : ''; ?>
                        >Any</option>
                    </select>
                </div>

                @php
                    $total = 0;
                @endphp
                <div class="search-option">
                    <label for="property_price">Property Price</label>
                    <select name="property_price" id="property_price">
                        <option value="UP TO 100K" 
                            <?php echo (isset($_GET['property_price']) && $_GET['property_price'] == 'UP TO 100K' )? 'selected' : ''; ?>
                        >UP TO 100K</option>
                        <option value="UP TO 200K" 
                            <?php echo (isset($_GET['property_price']) && $_GET['property_price'] == 'UP TO 200K' )? 'selected' : ''; ?>
                        >UP TO 200K</option>
                        <option value="UP TO 300K" 
                            <?php echo (isset($_GET['property_price']) && $_GET['property_price'] == 'UP TO 300K' )? 'selected' : ''; ?>
                        >UP TO 300K</option>
                        <option value="UP TO 400K" 
                            <?php echo (isset($_GET['property_price']) && $_GET['property_price'] == 'UP TO 400K' )? 'selected' : ''; ?>
                        >UP TO 400K</option>
                        <option value="400K+" 
                            <?php echo (isset($_GET['property_price']) && $_GET['property_price'] == '400K+' )? 'selected' : ''; ?>
                        >400K+</option>
                        <option value="Any"
                            <?php echo ((isset($_GET['property_price']) && $_GET['property_price'] == 'Any' ) || !isset($_GET['property_price']) )? 'selected' : ''; ?>
                        >Any</option>
                    </select>
                </div>

                <div class="search-option">
                        <label for="min-area">Min Area <span>(Sq m)</span></label>
                        <input 
                            type="text" 
                            name="min-area" 
                            id="min-area"
                            value="<?php echo isset($_GET['min-area']) ? $_GET['min-area'] : '' ?>"
                            pattern="[0-9]+"
                            placeholder="Any" 
                            title="Please only provide digits!">
                </div>

                <div class="search-option">
                    <label for="max-area">Max Area <span>(Sq m)</span></label>
                    <input 
                        type="text" 
                        name="max-area" 
                        id="max-area" 
                        value="<?php echo isset($_GET['max-area']) ? $_GET['max-area'] : '' ?>"
                        pattern="[0-9]+"
                        placeholder="Any" 
                        title="Please only provide digits!">
                </div>

                <div class="search-option">
                    <label for="keyword_search">Keyword</label>
                    <input type="text" name="keyword_search" id="keyword_search" value="<?php echo isset($_GET['keyword_search']) ? $_GET['keyword_search'] : '' ?>" 
                        placeholder="Any">
                </div>

                <div class="search-option">
                    <label for="property-id-txt">Property ID</label>
                    <input type="text" name="property_id" id="property-id-txt" 
                        value="<?php echo isset($_GET['property_id']) ? $_GET['property_id'] : '' ?>" placeholder="Any" autocomplete="off">
                </div>

                <div class="search-below search-button">
                    <input name="search" id="sbtn" type="submit" value="Search" class="btn btn-clr" disabled="disabled">
                    <input type="button" value="Reset" class="btn btn-clr" onclick=" if(confirm('Are you sure?')) window.location.href = '/properties';">
                </div>

                <div class="show-more-features" onclick="toggleFeatures();">
                    <i class="fa fa-search-plus"></i>
                    Search for specific features
                </div>
                <div class="hide collapsed" id="specific-features" style="display:none; ">
                    @php
                        $fn = 0;
                    @endphp
                    @foreach ($features as $feature)
                    @php
                        $checked = "";
                        if( isset($_GET['features']) ){
                            $ftt = $_GET['features'];
                            foreach ($ftt as $ftrv) {
                                if($feature == $ftrv){
                                    $checked = "checked";
                                    break;
                                }
                            }
                        }
                    @endphp
                        <div class="option-bar" style="margin:10px 0 0 0; font-size:14px;">
                                <input type="checkbox" id="{{ $fn }}" name="features[]" value="{{ $feature }}" {{ $checked }}>
                            <label for="{{ $fn }}">{{ $feature }} <small>({{ $frq[$feature] }})</small></label>
                        </div>
                        @php
                            $fn += 1;
                        @endphp
                    @endforeach
                </div>
            </form>
        </div>
</div>

@endsection
