
<style>
    .carousel-item{
        height: 460px !important;
    }
    .carousel-control-next{
        right: -60px;
    }
    .carousel-control-prev{
        left: -60px;
    }
    .carousel-indicators .active{
        background-color: {{ setting('site.color_accent') }};
    }

    @media (max-width:1000px){
        .carousel-item{
            height: auto !important;
        }
        .carousel-control-next{
            right: 0;
        }
        .carousel-control-prev{
            left: 0;
        }

    }

    @media (min-width:1500px){
        .carousel-item{
            height: 600px !important;
        }
    }
</style>

<section id="slider" style="margin-top:30px;">
    @php
        function endsWith($string, $endString) 
        { 
            $len = strlen($endString); 
            if ($len == 0) { 
                return true; 
            } 
            return (substr($string, -$len) === $endString); 
        } 
        $files = Storage::files('public/slider');
        $images = [];
        foreach ($files as $file) {
            if(endsWith($file,'jpg') || endsWith($file,'jpeg') || endsWith($file,'png') 
                || endsWith($file,'JPG') || endsWith($file,'JPEG') || endsWith($file,'PNG'))
            {
                $file = explode("/", $file);
                $image = "";
                for($i=1;$i<count($file);$i=$i+1){
                    $image = $image.$file[$i]."/";
                }
                $images[] = $image;
            }
        }
        $slide_count = count($images);
        // echo '<pre>'. var_export($images, true). '</pre>';
    @endphp
    @if ($slide_count > 0)
        <div
            id="carouselIndicators"
            class="carousel slide"
            data-ride="carousel"
            data-interval="{{ setting('site.slide-interval') }}"
        >
            <ol class="carousel-indicators" style="z-index:2;">
                @for ($i = 0; $i < $slide_count; $i++)
                    @if ($i == 0)
                        <li class="active" data-target="#carouselIndicators" data-slide-to="0"></li>
                    @else
                        <li data-target="#carouselIndicators" data-slide-to="{{ $i }}"></li>
                    @endif
                @endfor
            </ol>
            <div class="carousel-inner">
                @for ($i = 0; $i < $slide_count; $i++)
                    @if($i == 0)
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ asset('storage/'.$images[$i]) }}" alt="Slide {{ $i+1 }}" />
                        </div>
                    @else
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('storage/'.$images[$i]) }}" alt="Slide {{ $i+1 }}" />
                        </div>
                    @endif
                @endfor
            </div>
            <a
                class="carousel-control-prev"
                href="#carouselIndicators"
                role="button"
                data-slide="prev"
            >
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a
                class="carousel-control-next"
                href="#carouselIndicators"
                role="button"
                data-slide="next"
            >
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div> 
    @endif

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" ></script>
</section>
