<section id="latestnews">

    <style>
            .news_posts_area {
                margin: 0;
                padding: 86px 0 60px 0;
                width: 100%;
                background: #fff;
            }

            .news_posts_area h1 {
                margin: 0 0 22px 0;
                padding: 0;
                font-size: 24px;
                color: {{ setting('site.color_accent') }};
                text-transform: uppercase;
                font-family: "latobold";
            }
            .devider {
                margin: 0 0 30px 0;
                padding: 0;
                float: left;
                width: 100%;
                height: 3px;
                max-width: 97px;
                background: #000;
            }
            .news_post {
                margin-bottom: 40px;
                border: 1px solid {{ setting('site.color_accent') }};
                padding:10px;
            }

            figure {
                border: 0 solid #fff;
                cursor: pointer;
                display: block;
                height: auto;
                margin: 0;
                overflow: hidden;
                padding: 0;
                position: relative;
                display: contents;
            }

            .news_post img {
                width: 100%;
                margin-bottom: 0px;
            }

            .figcaptionx {
                -ms-filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
                filter: alpha(opacity=0);
                opacity: 0;
                position: absolute;
                /* height: 100%; */
                width: 100%;
                background: transparent;
                color: {{ setting('site.color_accent') }};
                /* -webkit-transition: all .3s ease;
                -moz-transition: all .3s ease;
                -o-transition: all .3s ease;
                -ms-transition: all .3s ease;
                transition: all .3s ease;
                -webkit-transition-delay: .3s;
                -moz-transition-delay: .3s;
                -o-transition-delay: .3s;
                -ms-transition-delay: .3s;
                transition-delay: .3s; */
                text-align: center;
                vertical-align: middle;
            }

            .figcaptionx h3 {
                color: {{ setting('site.color_accent') }};
                font-size: 14px;
                font-weight: 400;
                margin-bottom: 0;
                margin-top: 28%;
                padding: 0;
                position: relative;
                text-transform: uppercase;
                font-weight:bold;
            }

            .figcaptionx h3 a {
                background: {{ setting('site.color_secondary') }} none repeat scroll 0 0;
                color: {{ setting('site.color_accent') }} !important;
                margin: 0;
                padding: 12px 20px;
                text-decoration: none !important;
            }

            .news_post h2 {
                color: {{ setting('site.color_accent') }};
                font-family: "latobold";
                font-size: 16px;
                line-height: 22px;
                margin: 15px 0 10px;
                padding: 0;
                text-transform: uppercase;
            }

            .news_post p {
                margin: 0;
                padding: 0;
                font-size: 16px;
                color: {{ setting('site.color_secondary') }};
                text-transform: uppercase;
                font-family: 'latolight';
            }

            figure:hover .figcaptionx {
                -ms-filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=100);
                /* filter: alpha(opacity=100); */
                opacity: 1;
                top: 0;
            }

    </style>

    <script>
        function showReadMore(el){
            el.childNodes[3].style.opacity = '1';
            el.childNodes[3].style.top = '0';
        }

        function removeReadMore(el){
            el.childNodes[3].style.opacity = '0';
        }
    </script>

    <div class="news_posts_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1>Latest News</h1>

                    <div class="devider"></div>

                    <div class="clearfix"></div>
                </div>

                <div class="news_posts row" style="margin-left:0; margin-right:0;">
                    
                    
                    @foreach ($news as $item)
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="news_post">
                                <figure >
                                    <img
                                        src="{{ asset('storage/'.$item->image) }}"
                                        alt="{{ $item->title }}"
                                        class="img-responsive"
                                    />
    
                                    <figcaption class="figcaptionx">
                                        <h3>
                                            <a
                                                href="/news/{{ $item->slug }}"
                                                >Read More</a
                                            >
                                        </h3>
                                    </figcaption>
                                </figure>
    
                                <h2>{{ $item->title }}</h2>

                                @php
                                    $date = date(strtotime($item->created_at));
                                    $date = getdate($date);
                                    $month = $date["month"];
                                    $day = $date["mday"];
                                    $year = $date["year"];
                                @endphp
    
                                <p>{{ $day." ".$month." ".$year }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
