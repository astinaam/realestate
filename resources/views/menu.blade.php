{{-- @extends('layouts.app') --}}

{{-- @section('menu') --}}

<link rel="stylesheet" href="{{ asset('css/menu_css.css') }}">
<div class="header-wrapper" style="top: auto;">
    <div class="container" style="">
        <header id="header" class="clearfix">
            <div id="header-top" class="clearfix" style="margin-bottom: 30px; ">
                <h2 id="contact-address">
                    <i class="fas fa-map-marker-alt"></i>
                    S.P Business Center, 17 Neofytou Nikolaide & Kilkis str,Paphos. 3rd Flr, Office 301
                </h2>
                <h2 id="contact-email">
                    <i class="fas fa-envelope"></i>
                    <a href="mailto:info@kpphomes.com">info@kpphomes.com</a>
                    &nbsp;
                    <i class="fas fa-phone-volume"></i>
                    Call us at :
                    <a href="tel:+357 22262440">+357 22262440</a> or
                    <a href="tel:+357 99545577 ">+357 99545577</a>
                </h2>
                
                <!-- Social Navigation -->
                <ul class="social_networks clearfix">
                    <li class="facebook">
                        <a
                            target="_blank"
                            href="{{ setting('site.social-fb') }}"
                            >
                            <i class="fab fa-facebook-f" style="color:white;"></i>
                        </a>
                    </li>
                    <li class="twitter">
                        <a
                            target="_blank"
                            href="{{ setting('site.socila-tw') }}"
                            >
                            <i class="fab fa-twitter" style="color:white;"></i>
                        </a>
                    </li>
                    <li class="linkedin">
                        <a
                            target="_blank"
                            href="{{ setting('site.socila-ln') }}"
                            >
                            <i class="fab fa-linkedin-in" style="color:white;"></i>
                        </a>
                    </li>
                    <li class="gplus">
                        <a
                            target="_blank"
                            href="{{ setting('site.socila-gp') }}"
                            >
                            <i class="fab fa-google-plus-g" style="color:white;"></i>
                        </a>
                    </li>
                    <li class="youtube">
                        <a
                            target="_blank"
                            href="{{ setting('site.social-yt') }}"
                        >
                        <i class="fab fa-youtube" style="color:white;"></i>
                        </a>
                    </li>
                    <li class="skype">
                        <a
                            target="_blank"
                            href="skype:{{ setting('site.social-sk') }}"
                        >
                        <i class="fab fa-skype" style="color:white;"></i>
                        </a>
                    </li>
                </ul>

                <div class="user-nav clearfix"></div>
            </div>

            <div id="logo" style="margin-bottom:80px;">
            </div>
            <!-- Logo -->
            <div class="logo">
                <a
                    class="logo-image"
                    title="{{   setting('site.title') }}"
                    href="/"
                >
                    <img
                        src="{{   asset('storage/'.setting('site.logo')) }}"
                        alt="{{   setting('site.title') }}"
                        {{-- style="height:60px;" --}}
                    />
                </a>
                <h2 class="logo-heading only-for-print">
                    <a
                        href="/"
                        title="{{   setting('site.title') }}"
                    >
                        {{ setting('site.title') }}
                    </a>
                </h2>
            </div>

            <div class="menu-and-contact-wrap">
                <nav class="main-menu">
                    <div class="menu-main-menu-container">
                        <ul id="menu-main-menu" class="clearfix">
                           {{ menu('global_menu','layouts.menu_template') }}
                        </ul>
                        <script>
                            function goTo(link){
                                window.location.href = link;
                            }

                            
                        </script>
                        <select class="responsive-nav" onchange="goTo(this.value);">
                            <option value="" selected="">Go to...</option>
                            {{ menu('global_menu','layouts.menu_template_mobile') }}
                        </select>
                    </div>
                </nav>
                <!-- End Main Menu -->
            </div>
        </header>
    </div>
    <!-- End Header Container -->
</div>
{{-- @endsection --}}
