@extends('layouts.app') @section('content')

<style>
.inner_content_area {
    margin: 0;
    padding: 60px 0 30px 0;
    width: 100%;
    /* background: #FFF; */
}

.blog_post {
    margin: 0 0 40px 0;
    padding: 0;
    border: 1px solid #cecece;
}

.blog_post img {
    width: 100%;
}

.blog_post_detail {
    background: white none repeat scroll 0 0;
    margin: 0;
    padding: 40px;
    width: 100%;
}

.blog_post_detail h3 {
    margin: 0 0 10px 0;
    padding: 0;
    font-size: 24px;
    color: #315490;
    text-transform: uppercase;
    font-family: 'latobold';
}

.blog_post_detail ul {
    margin: 0 0 18px 0;
    padding: 0;
}

.blog_post_detail p {
    margin: 0 0 40px 0;
    padding: 0;
    font-size: 16px;
    color: #5c5c5c;
    line-height: 30px;
}

.digital_post {
    background: white none repeat scroll 0 0;
    border: 1px solid #cecece;
    margin: 0 0 25px;
    padding: 0;
    text-align: center;
}

.digital_post img {
    width: 100%;
}

.digital_post_detail {
    margin: 0;
    padding: 30px 30px 48px 30px;
    border-bottom: 3px solid #315490;
}

.blog_post_detail li{
    list-style: none;
}

.digital_post_detail p {
    margin: 0;
    padding: 0;
    font-size: 16px;
    color: #5c5c5c;
    text-align: left;
    line-height: 28px;
}

.search_area {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #cecece;
    float: left;
    height: 55px;
    margin: 0 0 25px;
    padding: 0;
    width: 100%;
}

.cate_area {
    border: 1px solid #cecece;
    float: left;
    margin: 0 0 25px;
    padding: 0;
    text-align: center;
    width: 100%;
    background: #FFF;
}
.cate_area h1 {
    border-bottom: 1px solid #cecece;
    color: #315490;
    float: left;
    font-family: 'latobold';
    font-size: 20px;
    margin: 0;
    padding: 20px 30px;
    text-align: left;
    text-transform: uppercase;
    width: 100%;
    text-align: center;
}

.cate_area h5 {
    float: left;
    width: 100%;
    border-bottom: 1px solid #ccc;
    padding-bottom: 5px;
    font-size: 17px;
    line-height: 36px;
    margin-bottom: 0;
}

a {
    color: #337ab7;
    text-decoration: none;
}

</style>


<div class="inner_content_area">
    <div class="container">
        {{-- <div class="row"> --}}
            <div class="blog_content row">
                <div class="col-lg-8 col-md-8">
                    <div class="blog_left_section">
                        <div class="blog_post">
                            <img
                                src="{{ asset('storage/'.$post->image) }}"
                                alt="{{ $post->title }}"
                                class="img-responsive"
                            />
                            @php
                                $date = date(strtotime($post->created_at));
                                $date = getdate($date);
                                $month = $date["month"];
                                $day = $date["mday"];
                                $year = $date["year"];
                            @endphp
                            <div class="blog_post_detail">
                                <h3>{{ $post->title }}</h3>
                                <ul>
                                    <li>
                                            <i class="far fa-clock"></i>&nbsp;&nbsp;{{ $month." ".$day.", ".$year }}
                                    </li>
                                </ul>
                                <p></p>
                                {!! $post->body !!}
                                <p></p>

                                <br>
                                <div title="Share this post">
                                        <div class="addthis_inline_share_toolbox"></div>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="blog_sidebar">
                        {{-- <aside id="text-9" class="widget widget_text">
                            <div class="textwidget">
                                <div class="digital_post">
                                    <img
                                        src="http://www.kppcorporate.com/wp-content/uploads/2017/04/sidebar_banner.jpg"
                                        alt=""
                                        class="img-responsive"
                                    />
                                    <div class="digital_post_detail">
                                        <h2>KPP Corporate Services</h2>
                                        <p>
                                            We are a Corporate Services provider
                                            in Cyprus handling Worldwide Company
                                            Formation, including Cyprus, UAE and
                                            all other Offshore Zones.We provide
                                            Nominee, Trustee &amp; Secretarial
                                            Services, together with virtual
                                            office services and registered
                                            office services.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </aside> --}}

                        
                        <div class="cate_area">
                            <h1>Latest News</h1>
                            

                            @foreach ($latest as $item)
                                <h5>
                                    <a
                                        href="/news/{{ $item->slug }}"
                                        >{{ $item->title }}</a
                                    >
                                </h5>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        {{-- </div> --}}
    </div>
</div>
@endsection
