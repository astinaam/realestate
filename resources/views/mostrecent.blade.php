<section id="mosrecent" style="padding-top:50px; padding-bottom:50px; background-color:white;">
        <style>
                .title h1 {
                    margin: 0 0 22px 0;
                    padding: 0;
                    font-size: 24px;
                    color: {{ setting('site.color_accent') }};
                    text-transform: uppercase;
                    /* color: #006; */
                    font-family: "latobold";
                }
                .devider {
                    margin: 0 0 30px 0;
                    padding: 0;
                    float: left;
                    width: 100%;
                    height: 3px;
                    max-width: 97px;
                    background: #000;
                }    
            
        </style>
        <div class="container">
                <div class="col-lg-12 col-md-12 title" style="padding-left:0;">
                        <h1>Most Recently Listed Properties</h1>
    
                        <div class="devider"></div>
    
                        <div class="clearfix"></div>
                    </div>
                <div class="row">
                        @php
                            $count = 0;
                            $limit = setting('site.most-recent');
                        @endphp
                        @foreach ($mostrecent as $sim)
                            @php
                                if($count == $limit) break;
                                $images = json_decode($sim->property_images);
                                if(!$images || count($images) == 0){
                                    $images[] = "default_image.jpg";
                                }
                                $friendly_url = is_null($sim->slug) ? $sim->id : $sim->slug;
                                $count++;
                            @endphp
                            <div class="col-sm-4 for_mobile2" style="border: 1px solid #CE9B2B; background-color:#eee; padding:10px; margin-top:30px;"
                                >
                                <div class="similar_items">
                                    {{-- <div class="title_x">
                                        <div class="property_city">
                                            <a href="{{ url('properties/'.$friendly_url) }}">{{ $sim->property_city }}</a>
                                        </div>
                                    </div> --}}
                                    <figure>
                                        <a href="{{ url('properties/'.$friendly_url) }}">
                                            <img src="{{ asset('storage/'.$images[0]) }}" alt="">
                                        </a>
                                    </figure>
                                    
                                    <!-- We display the price here -->
                                    <span id="price">&euro;{{ number_format($sim->property_price, 2) }} - For {{ $sim->property_type}}</span>
                                    <h4>
                                        <a href="{{ url('properties/'.$friendly_url) }}">
                                                {{ $sim->property_title }}
                                        </a>
                                    </h4>
                                    <p>{{ html_entity_decode(strip_tags(mb_substr($sim->property_description,0,70))) }}...</p>
                                    <div class="similar_items_title">
                                        <div class="more_detail">
                                            <a class="more-details-x" href="">
                                                More Details <i class="fa fa-caret-right"></i>
                                            </a>
                                        </div>
                                        <div class="prop_id" style="margin-top:-9px;">
                                                <h6>{{ $sim->property_id }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
        </div>
</section>