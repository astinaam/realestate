@extends('layouts.app')

@section('searchbar')

<div class="search_sub_wrapper">
    <div class="search_anchor">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="search" placeholder="Search" style="border-radius:0;">
                </div>
                <div class="col-sm-8">
                   
                    <div class="form-inline input-goup" style="float:right;">
                            <li class="dropdown form-control" style="border-radius:0;">
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Search For Specific Feature<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li>Separate the features with single comma</li>
                                        <li>
                                            <input type="text" class="form-control" onkeyup="getFeaturesFromField(this);"
                                            name="features" placeholder="Enter Features" style="width: 100%;">
                                        </li>
                                        <li id="show_features_ongoing">

                                        </li>
                                    </ul>
                            </li>
                            &nbsp;
                           <select class="form-control" name="gallery_sorter" id="galler_sorter" style="border-radius:0;">
                               <option value="0">DATE : NEW TO OLD</option>
                               <option value="0">DATE : OLD TO NEW</option>
                               <option value="0">PRICE : LOW TO HIGH</option>
                               <option value="0">PRICE : HIGH TO LOW</option>
                           </select>
                           &nbsp;
                           <div class="input-group-btn">
                                <button type="button" class="btn btn-primary" style="border-radius:0;">
                                    <strong>Search</strong>
                                </button>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
        <div class="row">
    
            <div class="parameters col-sm-3 col-xs-12">
    
                 <!-- <div class="search_bar_item"> -->
                        <li  class="dropdown form-control" style="width:150px;border-radius:0;">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Property Type<b class="caret"></b></a>
                                <ul id="ptype" class="dropdown-menu">
                                    <li> <input type="checkbox" name="property_type[]"> Apartment</li>
                                    <li> <input type="checkbox" name="property_type[]"> Family House</li>
                                    <li> <input type="checkbox" name="property_type[]"> Villa</li>
                                </ul>
                            </li>
                        <div class="selected_filters_show">
                            <span>Any</span>
                        </div>
                        <!-- </div> -->
                        &nbsp;
                        <!-- <div class="search_bar_item"> -->
                            <li id="pstatus" class="dropdown form-control" style="width:150px;border-radius:0;">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Property Status<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li> <input type="checkbox" name="property_status[]"> New</li>
                                    <li> <input type="checkbox" name="property_status[]"> Resale</li>
                                    <li> <input type="checkbox" name="property_status[]"> Offplan</li>
                                    <li> <input type="checkbox" name="property_status[]"> Under Construction</li>
                                </ul>
                            </li>
                            <div class="selected_filters_show">
                                    <span>Any</span>
                                </div>
                        <!-- </div> -->
    
                        &nbsp;
                        <li class="dropdown form-control" style="width:150px;border-radius:0;">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Price<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li> <input type="checkbox" name="property_price[]"> UP TO 100K</li>
                                <li> <input type="checkbox" name="property_price[]"> UP TO 200K</li>
                                <li> <input type="checkbox" name="property_price[]"> UP TO 300K</li>
                                <li> <input type="checkbox" name="property_price[]"> UP TO 400K</li>
                                <li> <input type="checkbox" name="property_price[]"> 400K+</li>
                            </ul>
                        </li>
                        <div class="selected_filters_show">
                                <span>Any</span>
                            </div>
                        
                        &nbsp;
                        <li class="dropdown form-control" style="width:150px;border-radius:0;">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">City<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li> <input type="checkbox" name="property_city[]"> PAPHOS</li>
                                <li> <input type="checkbox" name="property_city[]"> LIMASSOL</li>
                                <li> <input type="checkbox" name="property_city[]"> NICOSIA</li>
                                <li> <input type="checkbox" name="property_city[]"> LARNACA</li>
                                <li> <input type="checkbox" name="property_city[]"> FAMAGUSTA</li>
                            </ul>
                        </li>
                        <div class="selected_filters_show">
                                <span>Any</span>
                            </div>
    
                        &nbsp;
                        <li class="dropdown form-control" style="width:150px;border-radius:0;">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Location<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li> <input type="checkbox" name="property_city[]"> PAPHOS</li>
                                <li> <input type="checkbox" name="property_city[]"> LIMASSOL</li>
                                <li> <input type="checkbox" name="property_city[]"> NICOSIA</li>
                                <li> <input type="checkbox" name="property_city[]"> LARNACA</li>
                                <li> <input type="checkbox" name="property_city[]"> FAMAGUSTA</li>
                            </ul>
                        </li>
                        <div class="selected_filters_show">
                                <span>Any</span>
                            </div>
    
                        &nbsp;
                        <li class="dropdown form-control" style="width:150px;border-radius:0;">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Bedrooms<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li> <input type="checkbox" name="property_bedrooms[]" value="studio"> Studio</li>
                                <li> <input type="checkbox" name="property_bedrooms[]" value="1"> 1</li>
                                <li> <input type="checkbox" name="property_bedrooms[]" value="2"> 2</li>
                                <li> <input type="checkbox" name="property_bedrooms[]" value="3"> 3</li>
                                <li> <input type="checkbox" name="property_bedrooms[]" value="4"> 4</li>
                                <li> <input type="checkbox" name="property_bedrooms[]" value="4+"> 4+</li>
                            </ul>
                        </li>
                        <div class="selected_filters_show">
                                <span>Any</span>
                        </div>
    
                        
                        &nbsp;
                        <li class="dropdown form-control" style="width:150px;border-radius:0;">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Square Meters<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li> <input type="number" name="property_square_meters[]" min="0" placeholder="Min"></li>
                                <li> <input type="number" name="property_square_meters[]" min="1" placeholder="Max"></li>
                            </ul>
                        </li>
                        <div class="selected_filters_show">
                                <span>Any</span>
                                <span>Apartment</span>
                                <span>Family House</span>
                                <span>Under Contruction</span>
                                <span>UP TO 400K</span>
                        </div>
    
    
            </div>
    
            <div class="property_gallery banners1 col-sm-9 col-xs-12">
                    <div class="search_info" style="margin-top:10px; margin-bottom:30px;">
                            <h3>
                                {{ count($properties) }} Properties Found!
                            </h3>
                        </div>
                <div class="row">
                    @foreach ($properties as $property)
                        @php
                            $imgs = json_decode($property->property_images);
                            if(!$imgs){
                                $imgs[] = "default_image.jpg";
                            }
                        @endphp 
                        <div class="col-sm-4 col-xs-1 item">
                            <a href="{!! route('properties.show',[$property->id]) !!}">
                                
                                <img
                                    src="{{ asset('storage/'.$imgs[0]) }}"
                                    class="img-responsive"
                                    alt=""
                                    title=""
                                    width="300"
                                    height="300"
                                />
                                <div class="title">
                                    <div class="capt">
                                        <p>ID : {{ $property->property_id }}</p>
                                        <h3>{{ $property->property_title }}</h3>
                                        <h4>&euro;{{ number_format($property->property_price, 2) }}</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                            
                    @endforeach
                </div>
            </div>
            
        </div>
    </div>


@endsection