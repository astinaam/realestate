@extends('layouts.app')

@section('content')

<style>

</style>

{{-- <div class="testimonial-quote group right">
        <div class="quote-container">
            <div>
                <blockquote>
                    <p>Overall, fantastic! I'd recommend them to anyone looking for a creative, thoughtful, and professional team.”</p>
                </blockquote>  
                <cite><span>Kristi Bruno</span><br>
                    Social Media Specialist<br>
                    American College of Chest Physicians
                </cite>
            </div>
        </div>
    </div>  --}}

<div class="container pt-5" style="padding-top: 5rem !important;">

    <h2>
      {{ $page->title }}
    </h2>
    <div class="page-content__wrap">
      {!! $page->body !!}
    </div>
</div>
@endsection