<?php

namespace App\Http\Controllers;

use App\Property;
use App\ContactSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\Post;


class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $slug = $this->getSlug($request);
        // echo '<pre>'. var_export($_GET['sorty'], true). '</pre>';
        // echo '<pre>'. var_export($request->all(), true). '</pre>';

        $all_properties = Property::all();
        $types = array();
        $status = array();
        $cities = array();
        $loc = array();
        $frequency = array();
        $ppage = setting('site.properties_per_page');

        $features = array();

        foreach($all_properties as $prop){
            $frequency[] = $prop->property_type;
            $frequency[] = $prop->property_city;
            $frequency[] = $prop->property_status;
            $frequency[] = $prop->property_location;

            $types[] = $prop->property_type;
            $status[] = $prop->property_status;
            $cities[] = $prop->property_city;
            $loc[] = $prop->property_location;

            $pf = $prop->property_features;
            $ftrs = explode(",",$pf);
            foreach($ftrs as $ftr){
                $ftr = trim($ftr);
                if(strlen($ftr) == 0) {
                    continue;
                }
                $features[] = $ftr;
                $frequency[] = $ftr;
            }

            $pf = $prop->property_extra;
            $ftrs = explode(",",$pf);
            foreach($ftrs as $ftr){
                $ftr = trim($ftr);
                if(strlen($ftr) == 0) {
                    continue;
                }
                $ftr = explode(":",$ftr);
                $ftr = trim($ftr[0]);
                if(strlen($ftr) == 0) {
                    continue;
                }

                $features[] = $ftr;
                $frequency[] = $ftr;
            }
        }

        $frequency = array_count_values($frequency);
        $types = array_unique($types);
        $status = array_unique($status);
        $cities = array_unique($cities);
        $loc = array_unique($loc);
        $features = array_unique($features);


        // searched items search
        $all_get = $request->all();

        $build_where = array();
        $build_orwhere = array();
        $for_features = array();

        $raw_query = "";
        $raw_where = "";

        $titles = "";
        $fquery = "";

        foreach($all_get as $sp => $val){
            if($val && $val != 'Any'){
                $where = array();
                $orwhere = array();

                if($sp == "page" || $sp == "sortby") continue;
                if($sp == "perpage") {
                    $ppage = $val;
                    continue;
                }

                
                if(substr($raw_where, -strlen("AND ")) !== "AND " && strlen($raw_where) > 0){
                    $raw_where .= " AND ";
                }
                

                if($sp == "min-area"){
                    $where[] = 'property_square_meters';
                    $where[] = '>=';
                    $where[] = $val;

                    $raw_where .= "property_square_meters >= $val";
                }
                else if($sp == "max-area"){
                    $where[] = 'property_square_meters';
                    $where[] = '<=';
                    $where[] = $val;

                    $raw_where .= " property_square_meters <= $val";
                }
                else if($sp == "property_price"){
                    if($val == "UP TO 100K"){
                        $where[] = 'property_price';
                        $where[] = '<=';
                        $where[] = 100000;
                        $raw_where .= " property_price <= 100000";
                    }
                    else if($val == "UP TO 200K"){
                        $where[] = 'property_price';
                        $where[] = '<=';
                        $where[] = 200000;
                        $raw_where .= " property_price <= 200000";
                    }
                    else if($val == "UP TO 300K"){
                        $where[] = 'property_price';
                        $where[] = '<=';
                        $where[] = 300000;

                        $raw_where .= " property_price <= 300000";
                    }
                    else if($val == "UP TO 400K"){
                        $where[] = 'property_price';
                        $where[] = '<=';
                        $where[] = 400000;

                        $raw_where .= " property_price <= 400000";
                    }
                    else if($val == "400K+"){
                        $where[] = 'property_price';
                        $where[] = '>=';
                        $where[] = 400000;

                        $raw_where .= " property_price >= 400000 ";
                    }
                    else if(is_numeric($val)){
                        $where[] = 'property_price';
                        $where[] = '=';
                        $where[] = $val;

                        $raw_where .= " property_price = $val ";
                    }
                }
                else if($sp == "property_bathrooms")
                {
                    if($val == "1"){
                        $where[] = 'property_bathrooms';
                        $where[] = '>=';
                        $where[] = 1;

                        $raw_where .= " property_bathrooms >= '1'";
                    }
                    else if($val == "2"){
                        $where[] = 'property_bathrooms';
                        $where[] = '>=';
                        $where[] = 2;

                        $raw_where .= " property_bathrooms >= '2'";
                    }
                    else if($val == "3"){
                        $where[] = 'property_bathrooms';
                        $where[] = '>=';
                        $where[] = 3;

                        $raw_where .= " property_bathrooms >= '3'";
                    }
                    else if($val == "4"){
                        $where[] = 'property_bathrooms';
                        $where[] = '>=';
                        $where[] = 4;

                        $raw_where .= " property_bathrooms >= '4'";
                    }
                    else if($val == "4+"){
                        $where[] = 'property_bathrooms';
                        $where[] = '>=';
                        $where[] = 5;

                        $raw_where .= " property_bathrooms >= '5'";
                    }
                    else if(is_numeric($val)){
                        $where[] = 'property_bathrooms';
                        $where[] = '=';
                        $where[] = $val;

                        $raw_where .= " property_bathrooms LIKE '$val'";
                    }
                }
                else if($sp == "property_bedrooms")
                {
                    if($val == "1"){
                        $where[] = 'property_bedrooms';
                        $where[] = '>=';
                        $where[] = 1;
                        $raw_where .= " property_bedrooms >= '1'";
                    }
                    else if($val == "2"){
                        $where[] = 'property_bedrooms';
                        $where[] = '>=';
                        $where[] = 2;
                        $raw_where .= " property_bedrooms >= '2'";
                    }
                    else if($val == "3"){
                        $where[] = 'property_bedrooms';
                        $where[] = '>=';
                        $where[] = 3;
                        $raw_where .= " property_bedrooms >= '3'";
                    }
                    else if($val == "4"){
                        $where[] = 'property_bedrooms';
                        $where[] = '>=';
                        $where[] = 4;
                        $raw_where .= " property_bedrooms >= '4'";
                    }
                    else if($val == "4+"){
                        $where[] = 'property_bedrooms';
                        $where[] = '>=';
                        $where[] = 5;
                        $raw_where .= " property_bedrooms >= '5'";
                    }
                    else if($val == "Studio")
                    {
                        $where[] = 'property_bedrooms';
                        $where[] = 'LIKE';
                        $where[] = '%Studio%';
                        $raw_where .= " property_bedrooms LIKE %Studio%";
                    }
                    else if(is_numeric($val)){
                        $where[] = 'property_bedrooms';
                        $where[] = '=';
                        $where[] = $val;
                        $raw_where .= " property_bedrooms LIKE '$val'";
                    }
                }
                else if($sp == "property_type"){
                    $where[] = 'property_type';
                    $where[] = 'LIKE';
                    $where[] = '%'.$val.'%';

                    $raw_where .= " property_type LIKE '%$val%'";
                }
                else if($sp == "property_status"){
                    $where[] = 'property_status';
                    $where[] = 'LIKE';
                    $where[] = '%'.$val.'%';

                    $raw_where .= " property_status LIKE '%$val%'";
                }
                else if($sp == "property_city"){
                    $where[] = 'property_city';
                    $where[] = 'LIKE';
                    $where[] = '%'.$val.'%';

                    $raw_where .= " property_city LIKE '%$val%'";
                }
                else if($sp == "property_location"){
                    $where[] = 'property_location';
                    $where[] = 'LIKE';
                    $where[] = '%'.$val.'%';

                    $raw_where .= " property_location LIKE '%$val%'";
                }
                else if($sp == "keyword_search"){
                    $orwhere[] = 'property_title';
                    $orwhere[] = 'ILIKE';
                    $orwhere[] = '%'.$val.'%';
                    $build_orwhere[] = $orwhere;

                    $titles = " ( property_title ILIKE '%$val%' OR ";

                    $orwhere = array();
                    $orwhere[] = 'property_description';
                    $orwhere[] = 'ILIKE';
                    $orwhere[] = '%'.$val.'%';
                    $build_orwhere[] = $orwhere;

                    $titles .= " property_description ILIKE '%$val%' OR ";

                    $orwhere = array();
                    $orwhere[] = 'property_additional_details';
                    $orwhere[] = 'ILIKE';
                    $orwhere[] = '%'.$val.'%';

                    $titles .= " property_additional_details ILIKE '%$val%' ) ";
                }
                else if($sp == "property_id"){
                    $where[] = 'property_id';
                    $where[] = 'ILIKE';
                    $where[] = '%'.$val.'%';

                    $raw_where .= " property_id ILIKE '%$val%'";
                }
                else if($sp == "features"){
                    foreach($val as $v){
                        $for_features[] = ['property_features','ILIKE','%'.$v.'%'];
                        $for_features[] = ['property_extra','ILIKE','%'.$v.'%'];

                        $nw = " ( property_features ILIKE '%$v%' OR property_extra ILIKE '%$v%' ) ";

                        if(strlen($fquery) > 0)
                            $fquery = $fquery." AND ".$nw;
                        else 
                            $fquery = $nw;
                    }
                }

                if(count($where))
                    $build_where[] = $where;
                if(count($orwhere))
                    $build_orwhere[] = $orwhere;
            }
        }

        if(strlen($fquery) > 0) $fquery = " ( ".$fquery." ) ";

        // echo $raw_where."XX";
        // echo $titles."XX";
        // echo $fquery."XX";

        $final = "";
        if(substr($raw_where, -strlen("AND ")) === "AND "){
            // // echo "FCK";
            // $final .= $raw_where.$titles." AND ".$fquery;
            if(strlen($raw_where) > 0){
                $final .= $raw_where;
            }

            if(strlen($titles) > 0 && strlen($raw_where))
                $final .=" ".$titles;
            else if(strlen($titles) > 0) $final .=" ".$titles;

            if(strlen($fquery) > 0 && (strlen($titles) > 0 || strlen($raw_where) > 0)) 
            {
                if(strlen($titles) > 0)
                    $final .= " AND ".$fquery;
                else{
                    $final .= " ".$fquery;
                }
            }
            else if(strlen($fquery) > 0)
                $final .= " ".$fquery;
        }
        else{
            if(strlen($raw_where) > 0){
                $final .= $raw_where;
            }
            if(strlen($titles) > 0 && strlen($raw_where))
                $final .=" AND ".$titles;
            else if(strlen($titles) > 0) $final .=" ".$titles;

            if(strlen($fquery) > 0 && (strlen($titles) > 0 || strlen($raw_where) > 0)) 
            {
                $final .= " AND ".$fquery;
            }
            else if(strlen($fquery) > 0)
                $final .= " ".$fquery;
        }

        if(strlen($final) > 0)
        {
            $final = "SELECT * FROM properties WHERE ".$final;

            if(isset($all_get['sortby'])){
                $sortby = $all_get['sortby'];

                if($sortby == "date-desc" || $sortby == "default"){
                    $final .= " ORDER BY created_at DESC";
                }
                else if($sortby == "date-asc"){
                    $final .= " ORDER BY created_at ASC";
                }
                else if($sortby == "price-asc"){
                    $final .= " ORDER BY property_price ASC";
                }
                else if($sortby == "price-desc"){
                    $final .= " ORDER BY property_price DESC";
                }
            }

            $properties = DB::select( DB::raw($final) );

            $cpage = isset($all_get['page']) ? $all_get['page'] : 1;
            $offset = ($cpage * $ppage) - $ppage;

            $properties =  new \Illuminate\Pagination\LengthAwarePaginator(array_slice($properties, $offset, $ppage, true), count($properties), $ppage, $cpage,
                    ['path' => $request->url(), 'query' => $request->query()]);

            // echo '<pre>'. var_export($final, true). '</pre>';
            // echo '<pre>'. var_export($properties, true). '</pre>';
        }
        else{

            if(isset($all_get['sortby'])){
                $sortby = $all_get['sortby'];

                if($sortby == "date-desc" || $sortby == "default"){
                    $properties = Property::orderBy('created_at','DESC')->paginate($ppage);
                }
                else if($sortby == "date-asc"){
                    $properties = Property::orderBy('created_at','ASC')->paginate($ppage);
                }
                else if($sortby == "price-asc"){
                    $properties = Property::orderBy('property_price','ASC')->paginate($ppage);
                }
                else if($sortby == "price-desc"){
                    $properties = Property::orderBy('property_price','DESC')->paginate($ppage);
                }
            }
            else
                $properties = Property::paginate($ppage);
        }

        $properties->appends($all_get)->links();
        
        
        // echo '<pre>'. var_export($properties, true). '</pre>';
        
        if( count($all_get) == 0 )
            return view('property')->with('properties',$properties)->with('cities', $cities)->with('types', $types)->with('statuses', $status)->with('frq',$frequency)->with('loc',$loc)->with('features',$features);
        return view('propertywsbr')->with('properties',$properties)->with('cities', $cities)->with('types', $types)->with('statuses', $status)->with('frq',$frequency)->with('loc',$loc)->with('features',$features);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function homePage()
    {
        $all_properties = Property::all();
        $types = array();
        $status = array();
        $cities = array();
        $loc = array();
        $frequency = array();

        $features = array();

        foreach($all_properties as $prop){
            $frequency[] = $prop->property_type;
            $frequency[] = $prop->property_city;
            $frequency[] = $prop->property_status;
            $frequency[] = $prop->property_location;

            $types[] = $prop->property_type;
            $status[] = $prop->property_status;
            $cities[] = $prop->property_city;
            $loc[] = $prop->property_location;

            $pf = $prop->property_features;
            $ftrs = explode(",",$pf);
            foreach($ftrs as $ftr){
                $ftr = trim($ftr);
                if(strlen($ftr) == 0) {
                    continue;
                }
                $features[] = $ftr;
                $frequency[] = $ftr;
            }

            $pf = $prop->property_extra;
            $ftrs = explode(",",$pf);
            foreach($ftrs as $ftr){
                $ftr = trim($ftr);
                if(strlen($ftr) == 0) {
                    continue;
                }
                $ftr = explode(":",$ftr);
                $ftr = trim($ftr[0]);
                if(strlen($ftr) == 0) {
                    continue;
                }

                $features[] = $ftr;
                $frequency[] = $ftr;
            }
        }

        $frequency = array_count_values($frequency);
        $types = array_unique($types);
        $status = array_unique($status);
        $cities = array_unique($cities);
        $loc = array_unique($loc);
        $features = array_unique($features);

        $lnews = Post::where('status','PUBLISHED')->orderBy('created_at')->take(3)->get();

        // most recent
        $mostrecent = Property::orderBy('created_at')->take(20)->get();

        // featured
        $featured = Property::where('is_featured','YES')->take(20)->get();

        return view('index')->with('cities', $cities)
        ->with('types', $types)->with('statuses', $status)
        ->with('frq',$frequency)->with('loc',$loc)
        ->with('features',$features)
        ->with('news',$lnews)->with('mostrecent',$mostrecent)->with('featured',$featured);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if(is_numeric($id)){
            $property = Property::findOrfail($id);
        }
        else{
            $property = Property::where("slug","=",$id)->firstOrfail();
        }

        $contact_deatils = ContactSetting::orderBy('created_at','DESC')->first();

        $price = $property->property_price;
        // by price <= +10%
        $ten_percent = ($price / 100) * 30;
        $price = $price + $ten_percent;
        $price2 = $price - $ten_percent;
        // find similar properties
        // by type
        $properties1 = Property::where('property_type','=',$property->property_type)->where('id','<>',$property->id)//->where('property_price','>=',$price2)
                                ->where('property_price','<=',$price)->where('property_city','=',$property->property_city)->inRandomOrder()->get();
        // by status
        $properties2 = Property::where('property_status','=',$property->property_type)->where('id','<>',$property->id)//->where('property_price','>=',$price2)
                                ->where('property_price','<=',$price)->where('property_city','=',$property->property_city)->inRandomOrder()->get();
        // by city
        $properties3 = Property::where('property_city','=',$property->property_city)->where('property_price','<=',$price)//->where('property_price','>=',$price2)
                                ->where('id','<>',$property->id)->inRandomOrder()->get();
        // by location
        $properties4 = Property::where('property_location','=',$property->property_location)->where('property_price','<=',$price)//->where('property_price','>=',$price2)
                                ->where('id','<>',$property->id)->inRandomOrder()->get();

        $sofar = 0;
        $cnt = 0;
        $similar = array();
        $mk_unique = [];

        $sizes = array(count($properties1), count($properties2), count($properties3), count($properties4));
        $keep = array(
            count($properties1) => 1,
            count($properties2) => 2,
            count($properties3) => 3,
            count($properties4) => 4,
        );
        sort($sizes);

        $extra_two = 0;

        foreach($sizes as $item){
            $idx = $keep[$item];
            foreach(${"properties".$idx} as $p){
                // var_dump($cnt);
                // var_dump($sofar);
                // var_dump($extra_two);
                // var_dump($mk_unique);
                if($cnt >= $sofar + 1) {
                    if($extra_two < 2){
                        if(array_search($p->id, $mk_unique) === FALSE)
                        {
                            $similar[] = $p;
                            $mk_unique[] = $p->id;
                            $extra_two += 1;
                        }
                    }
                    break;
                }
                if(array_search($p->id, $mk_unique) === FALSE)
                {
                    $similar[] = $p;
                    $mk_unique[] = $p->id;
                    $cnt = $cnt + 1;
                }
                
            }
            $sofar = $sofar + 1;
        }

        // if(count($similar) <= 5){
        //     $pathaw = Property::where('property_location','=',$property->property_location)->OrWhere('property_price','<=',$price)
        //     ->OrWhere('property_price','>=',$price2)->OrWhere('property_status','=',$property->property_status)
        //     ->OrWhere('property_city','=',$property->property_city)->OrWhere('property_type','=',$property->property_type)
        //     ->where('id','<>',$property->id)->inRandomOrder()->get();
        //     $cnt = 0;
        //     $need = 6 - count($similar);
        //     foreach($pathaw as $pt){
        //         $similar[] = $pt;
        //         $cnt = $cnt + 1;
        //         if( $cnt == $need) break;
        //     }
        // }

        // echo '<pre>'. var_export($properties2, true). '</pre>';
        return view('single_property')->with('property', $property)->with('contact',$contact_deatils)->with('similar',$similar);
    }

    public function test(){
        return view('home');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function edit(Property $property)
    {
        echo $property;
    }

    /**
     * Get response of ajax request
     * 
     */
    public function getNavigation(Request $request)
    {
        // $slug = $this->getSlug($request);
        // echo '<pre>'. var_export($_GET['sorty'], true). '</pre>';
        // echo '<pre>'. var_export($request->all(), true). '</pre>';

        $all_properties = Property::all();
        $types = array();
        $status = array();
        $cities = array();
        $loc = array();
        $frequency = array();

        $features = array();

        foreach($all_properties as $prop){
            $frequency[] = $prop->property_type;
            $frequency[] = $prop->property_city;
            $frequency[] = $prop->property_status;
            $frequency[] = $prop->property_location;

            $types[] = $prop->property_type;
            $status[] = $prop->property_status;
            $cities[] = $prop->property_city;
            $loc[] = $prop->property_location;

            $pf = $prop->property_features;
            $ftrs = explode(",",$pf);
            foreach($ftrs as $ftr){
                $ftr = trim($ftr);
                if(strlen($ftr) == 0) {
                    continue;
                }
                $features[] = $ftr;
                $frequency[] = $ftr;
            }

            $pf = $prop->property_extra;
            $ftrs = explode(",",$pf);
            foreach($ftrs as $ftr){
                $ftr = trim($ftr);
                if(strlen($ftr) == 0) {
                    continue;
                }
                $ftr = explode(":",$ftr);
                $ftr = trim($ftr[0]);
                if(strlen($ftr) == 0) {
                    continue;
                }

                $features[] = $ftr;
                $frequency[] = $ftr;
            }
        }

        $frequency = array_count_values($frequency);
        $types = array_unique($types);
        $status = array_unique($status);
        $cities = array_unique($cities);
        $loc = array_unique($loc);
        $features = array_unique($features);


        // searched items search
        $all_get = $request->all();

        $build_where = array();
        $build_orwhere = array();
        $for_features = array();

        $raw_query = "";
        $raw_where = "";

        $titles = "";
        $fquery = "";

        foreach($all_get as $sp => $val){
            if($val && $val !='Any'){
                $where = array();
                $orwhere = array();

                if($sp == "page" || $sp == "sortby") continue;
                if($sp == "perpage") {
                    continue;
                }

                
                if(substr($raw_where, -strlen("AND ")) !== "AND " && strlen($raw_where) > 0){
                    $raw_where .= " AND ";
                }
                

                if($sp == "min-area"){
                    $where[] = 'property_square_meters';
                    $where[] = '>=';
                    $where[] = $val;

                    $raw_where .= "property_square_meters >= $val";
                }
                else if($sp == "max-area"){
                    $where[] = 'property_square_meters';
                    $where[] = '<=';
                    $where[] = $val;

                    $raw_where .= " property_square_meters <= $val";
                }
                else if($sp == "property_price"){
                    if($val == "UP TO 100K"){
                        $where[] = 'property_price';
                        $where[] = '<=';
                        $where[] = 100000;
                        $raw_where .= " property_price <= 100000";
                    }
                    else if($val == "UP TO 200K"){
                        $where[] = 'property_price';
                        $where[] = '<=';
                        $where[] = 200000;
                        $raw_where .= " property_price <= 200000";
                    }
                    else if($val == "UP TO 300K"){
                        $where[] = 'property_price';
                        $where[] = '<=';
                        $where[] = 300000;

                        $raw_where .= " property_price <= 300000";
                    }
                    else if($val == "UP TO 400K"){
                        $where[] = 'property_price';
                        $where[] = '<=';
                        $where[] = 400000;

                        $raw_where .= " property_price <= 400000";
                    }
                    else if($val == "400K+"){
                        $where[] = 'property_price';
                        $where[] = '>=';
                        $where[] = 400000;

                        $raw_where .= " property_price >= 400000 ";
                    }
                    else if(is_numeric($val)){
                        $where[] = 'property_price';
                        $where[] = '=';
                        $where[] = $val;

                        $raw_where .= " property_price = $val ";
                    }
                }
                else if($sp == "property_bathrooms")
                {
                    if($val == "1"){
                        $where[] = 'property_bathrooms';
                        $where[] = '>=';
                        $where[] = 1;

                        $raw_where .= " property_bathrooms >= '1'";
                    }
                    else if($val == "2"){
                        $where[] = 'property_bathrooms';
                        $where[] = '>=';
                        $where[] = 2;

                        $raw_where .= " property_bathrooms >= '2'";
                    }
                    else if($val == "3"){
                        $where[] = 'property_bathrooms';
                        $where[] = '>=';
                        $where[] = 3;

                        $raw_where .= " property_bathrooms >= '3'";
                    }
                    else if($val == "4"){
                        $where[] = 'property_bathrooms';
                        $where[] = '>=';
                        $where[] = 4;

                        $raw_where .= " property_bathrooms >= '4'";
                    }
                    else if($val == "4+"){
                        $where[] = 'property_bathrooms';
                        $where[] = '>=';
                        $where[] = 5;

                        $raw_where .= " property_bathrooms >= '5'";
                    }
                    else if(is_numeric($val)){
                        $where[] = 'property_bathrooms';
                        $where[] = '=';
                        $where[] = $val;

                        $raw_where .= " property_bathrooms LIKE '$val'";
                    }
                }
                else if($sp == "property_bedrooms")
                {
                    if($val == "1"){
                        $where[] = 'property_bedrooms';
                        $where[] = '>=';
                        $where[] = 1;
                        $raw_where .= " property_bedrooms >= '1'";
                    }
                    else if($val == "2"){
                        $where[] = 'property_bedrooms';
                        $where[] = '>=';
                        $where[] = 2;
                        $raw_where .= " property_bedrooms >= '2'";
                    }
                    else if($val == "3"){
                        $where[] = 'property_bedrooms';
                        $where[] = '>=';
                        $where[] = 3;
                        $raw_where .= " property_bedrooms >= '3'";
                    }
                    else if($val == "4"){
                        $where[] = 'property_bedrooms';
                        $where[] = '>=';
                        $where[] = 4;
                        $raw_where .= " property_bedrooms >= '4'";
                    }
                    else if($val == "4+"){
                        $where[] = 'property_bedrooms';
                        $where[] = '>=';
                        $where[] = 5;
                        $raw_where .= " property_bedrooms >= '5'";
                    }
                    else if($val == "Studio")
                    {
                        $where[] = 'property_bedrooms';
                        $where[] = 'LIKE';
                        $where[] = '%Studio%';
                        $raw_where .= " property_bedrooms ILIKE %Studio%";
                    }
                    else if(is_numeric($val)){
                        $where[] = 'property_bedrooms';
                        $where[] = '=';
                        $where[] = $val;
                        $raw_where .= " property_bedrooms LIKE '$val'";
                    }
                }
                else if($sp == "property_type"){
                    $where[] = 'property_type';
                    $where[] = 'LIKE';
                    $where[] = '%'.$val.'%';

                    $raw_where .= " property_type LIKE '%$val%'";
                }
                else if($sp == "property_status"){
                    $where[] = 'property_status';
                    $where[] = 'LIKE';
                    $where[] = '%'.$val.'%';

                    $raw_where .= " property_status LIKE '%$val%'";
                }
                else if($sp == "property_city"){
                    $where[] = 'property_city';
                    $where[] = 'LIKE';
                    $where[] = '%'.$val.'%';

                    $raw_where .= " property_city LIKE '%$val%'";
                }
                else if($sp == "property_location"){
                    $where[] = 'property_location';
                    $where[] = 'LIKE';
                    $where[] = '%'.$val.'%';

                    $raw_where .= " property_location LIKE '%$val%'";
                }
                else if($sp == "keyword_search"){
                    $orwhere[] = 'property_title';
                    $orwhere[] = 'ILIKE';
                    $orwhere[] = '%'.$val.'%';
                    $build_orwhere[] = $orwhere;

                    $titles = " ( property_title ILIKE '%$val%' OR ";

                    $orwhere = array();
                    $orwhere[] = 'property_description';
                    $orwhere[] = 'ILIKE';
                    $orwhere[] = '%'.$val.'%';
                    $build_orwhere[] = $orwhere;

                    $titles .= " property_description ILIKE '%$val%' OR ";

                    $orwhere = array();
                    $orwhere[] = 'property_additional_details';
                    $orwhere[] = 'ILIKE';
                    $orwhere[] = '%'.$val.'%';

                    $titles .= " property_additional_details ILIKE '%$val%' ) ";
                }
                else if($sp == "property_id"){
                    $where[] = 'property_id';
                    $where[] = 'ILIKE';
                    $where[] = '%'.$val.'%';

                    $raw_where .= " property_id ILIKE '%$val%'";
                }
                else if($sp == "features"){
                    foreach($val as $v){
                        $for_features[] = ['property_features','ILIKE','%'.$v.'%'];
                        $for_features[] = ['property_extra','ILIKE','%'.$v.'%'];

                        $nw = " ( property_features ILIKE '%$v%' OR property_extra ILIKE '%$v%' ) ";

                        if(strlen($fquery) > 0)
                            $fquery = $fquery." AND ".$nw;
                        else 
                            $fquery = $nw;
                    }
                }

                if(count($where))
                    $build_where[] = $where;
                if(count($orwhere))
                    $build_orwhere[] = $orwhere;
            }
        }

        if(strlen($fquery) > 0) $fquery = " ( ".$fquery." ) ";

        // echo $raw_where."XX";
        // echo $titles."XX";
        // echo $fquery."XX";

        $final = "";
        if(substr($raw_where, -strlen("AND ")) === "AND "){
            // // echo "FCK";
            // $final .= $raw_where.$titles." AND ".$fquery;
            if(strlen($raw_where) > 0){
                $final .= $raw_where;
            }

            if(strlen($titles) > 0 && strlen($raw_where))
                $final .=" ".$titles;
            else if(strlen($titles) > 0) $final .=" ".$titles;

            if(strlen($fquery) > 0 && (strlen($titles) > 0 || strlen($raw_where) > 0)) 
            {
                if(strlen($titles) > 0)
                    $final .= " AND ".$fquery;
                else{
                    $final .= " ".$fquery;
                }
            }
            else if(strlen($fquery) > 0)
                $final .= " ".$fquery;
        }
        else{
            if(strlen($raw_where) > 0){
                $final .= $raw_where;
            }
            if(strlen($titles) > 0 && strlen($raw_where))
                $final .=" AND ".$titles;
            else if(strlen($titles) > 0) $final .=" ".$titles;

            if(strlen($fquery) > 0 && (strlen($titles) > 0 || strlen($raw_where) > 0)) 
            {
                $final .= " AND ".$fquery;
            }
            else if(strlen($fquery) > 0)
                $final .= " ".$fquery;
        }

        if(strlen($final) > 0)
        {
            $final = "SELECT * FROM properties WHERE ".$final;

            if(isset($all_get['sortby'])){
                $sortby = $all_get['sortby'];

                if($sortby == "date-desc" || $sortby == "default"){
                    $final .= " ORDER BY created_at DESC";
                }
                else if($sortby == "date-asc"){
                    $final .= " ORDER BY created_at ASC";
                }
                else if($sortby == "price-asc"){
                    $final .= " ORDER BY property_price ASC";
                }
                else if($sortby == "price-desc"){
                    $final .= " ORDER BY property_price DESC";
                }
            }

            $properties = DB::select( DB::raw($final) );
        }
        else{

            if(isset($all_get['sortby'])){
                $sortby = $all_get['sortby'];

                if($sortby == "date-desc" || $sortby == "default"){
                    $properties = Property::orderBy('created_at','DESC')->get();
                }
                else if($sortby == "date-asc"){
                    $properties = Property::orderBy('created_at','ASC')->get();
                }
                else if($sortby == "price-asc"){
                    $properties = Property::orderBy('property_price','ASC')->get();
                }
                else if($sortby == "price-desc"){
                    $properties = Property::orderBy('property_price','DESC')->get();
                }
            }
            else
                $properties = Property::all();
        }
        
        // print like JSON
        $arr = [];
        for($i =0; $i < count($properties); $i++){
            $arr[] = $properties[$i]->id;
        }

        $arr = json_encode($arr);

        echo $arr;
        
        // echo '<pre>'. var_export($arr, true). '</pre>';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Property $property)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property $property)
    {
        //
    }
}
