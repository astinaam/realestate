<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;

class PostsController extends Controller
{
	public function index()
	{
        $posts = Post::where('status','PUBLISHED')->get();
        // echo '<pre>'. var_export($posts, true). '</pre>';
		return view('posts')->with('posts', $posts);
	}

	public function getPost($post_slug = null)
	{
		$post = Post::where('slug',$post_slug)->where('status','PUBLISHED')->firstOrFail();

		$latestPosts = Post::orderBy('created_at')->take(5)->get();
        // echo '<pre>'. var_export($post, true). '</pre>';
		return view('post')->with('post', $post)->with('latest',$latestPosts);
	}


}