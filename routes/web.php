<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/properties', 'PropertyController@index');
// Route::post('/properties', 'PropertyController@index');
// for ajax queries
Route::get('/properties/getNavigation', 'PropertyController@getNavigation');
Route::get('/', 'PropertyController@homePage');

// Route::get('/about', 'PropertyController@index');
// Route::get('/whycyprus', 'PropertyController@test');

Route::get('/properties/{slug}', 'PropertyController@show');



Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


// Route::resource('properties','PropertyController');


// TODO : Send Mail
Route::post('sendmail', function(Request $request){
    $data = array(
        'name' => $request->name,
         'mail'=>$request->email,
          'message'=>$request->message
    );
    $sendto = App\ContactSetting::orderBy('created_at')->first();
    $sendto = explode(",",$sendto);

    $tosend = array();
    foreach($sendto as $item){
        $item = trim($item);
        if($item){
            $tosend[] = $item;
        }
    }

    if(count($tosend) == 0) return;

    Mail::send('email', $data, function ($message) use($request) {

        $message->from($request->mail,$request->name);
        $message->to($tosend[0])->subject('Message From '.$request->name);

    });

    return "Your email has been sent successfully";
});
// for news/ posts
Route::get('news','PostsController@index');

Route::get('news/{slug}',[
    'uses' => 'PostsController@getPost'
])->where('slug', '([A-Za-z0-9\-\/]+)');

// for pages
Route::get('{slug}', [
	'uses' => 'PagesController@getPage'
])->where('slug', '([A-Za-z0-9\-\/]+)');