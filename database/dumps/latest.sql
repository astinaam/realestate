--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.categories (
    id integer NOT NULL,
    parent_id integer,
    "order" integer DEFAULT 1 NOT NULL,
    name character varying(191) NOT NULL,
    slug character varying(191) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.categories OWNER TO astinaam;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO astinaam;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: data_rows; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.data_rows (
    id integer NOT NULL,
    data_type_id integer NOT NULL,
    field character varying(191) NOT NULL,
    type character varying(191) NOT NULL,
    display_name character varying(191) NOT NULL,
    required boolean DEFAULT false NOT NULL,
    browse boolean DEFAULT true NOT NULL,
    read boolean DEFAULT true NOT NULL,
    edit boolean DEFAULT true NOT NULL,
    add boolean DEFAULT true NOT NULL,
    delete boolean DEFAULT true NOT NULL,
    details text,
    "order" integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.data_rows OWNER TO astinaam;

--
-- Name: data_rows_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.data_rows_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_rows_id_seq OWNER TO astinaam;

--
-- Name: data_rows_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.data_rows_id_seq OWNED BY public.data_rows.id;


--
-- Name: data_types; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.data_types (
    id integer NOT NULL,
    name character varying(191) NOT NULL,
    slug character varying(191) NOT NULL,
    display_name_singular character varying(191) NOT NULL,
    display_name_plural character varying(191) NOT NULL,
    icon character varying(191),
    model_name character varying(191),
    description character varying(191),
    generate_permissions boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    server_side smallint DEFAULT '0'::smallint NOT NULL,
    controller character varying(191),
    policy_name character varying(191),
    details text
);


ALTER TABLE public.data_types OWNER TO astinaam;

--
-- Name: data_types_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.data_types_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_types_id_seq OWNER TO astinaam;

--
-- Name: data_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.data_types_id_seq OWNED BY public.data_types.id;


--
-- Name: menu_items; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.menu_items (
    id integer NOT NULL,
    menu_id integer,
    title character varying(191) NOT NULL,
    url character varying(191) NOT NULL,
    target character varying(191) DEFAULT '_self'::character varying NOT NULL,
    icon_class character varying(191),
    color character varying(191),
    parent_id integer,
    "order" integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    route character varying(191),
    parameters text
);


ALTER TABLE public.menu_items OWNER TO astinaam;

--
-- Name: menu_items_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.menu_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menu_items_id_seq OWNER TO astinaam;

--
-- Name: menu_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.menu_items_id_seq OWNED BY public.menu_items.id;


--
-- Name: menus; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.menus (
    id integer NOT NULL,
    name character varying(191) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.menus OWNER TO astinaam;

--
-- Name: menus_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.menus_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menus_id_seq OWNER TO astinaam;

--
-- Name: menus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.menus_id_seq OWNED BY public.menus.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(191) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO astinaam;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO astinaam;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: pages; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.pages (
    id integer NOT NULL,
    author_id integer NOT NULL,
    title character varying(191) NOT NULL,
    excerpt text,
    body text,
    image character varying(191),
    slug character varying(191) NOT NULL,
    meta_description text,
    meta_keywords text,
    status character varying(255) DEFAULT 'INACTIVE'::character varying NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT pages_status_check CHECK (((status)::text = ANY ((ARRAY['ACTIVE'::character varying, 'INACTIVE'::character varying])::text[])))
);


ALTER TABLE public.pages OWNER TO astinaam;

--
-- Name: pages_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.pages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pages_id_seq OWNER TO astinaam;

--
-- Name: pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.pages_id_seq OWNED BY public.pages.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.password_resets (
    email character varying(191) NOT NULL,
    token character varying(191) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO astinaam;

--
-- Name: permission_role; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.permission_role (
    permission_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.permission_role OWNER TO astinaam;

--
-- Name: permissions; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.permissions (
    id integer NOT NULL,
    key character varying(191) NOT NULL,
    table_name character varying(191),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.permissions OWNER TO astinaam;

--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO astinaam;

--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- Name: posts; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.posts (
    id integer NOT NULL,
    author_id integer NOT NULL,
    category_id integer,
    title character varying(191) NOT NULL,
    seo_title character varying(191),
    excerpt text,
    body text NOT NULL,
    image character varying(191),
    slug character varying(191) NOT NULL,
    meta_description text,
    meta_keywords text,
    status character varying(255) DEFAULT 'DRAFT'::character varying NOT NULL,
    featured boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT posts_status_check CHECK (((status)::text = ANY ((ARRAY['PUBLISHED'::character varying, 'DRAFT'::character varying, 'PENDING'::character varying])::text[])))
);


ALTER TABLE public.posts OWNER TO astinaam;

--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.posts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_id_seq OWNER TO astinaam;

--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.posts_id_seq OWNED BY public.posts.id;


--
-- Name: properties; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.properties (
    id integer NOT NULL,
    property_type character varying(1000),
    property_status character varying(1000),
    property_features character varying(1000),
    property_price double precision,
    property_city character varying(1000),
    property_location character varying(10000),
    property_bedrooms character varying(1000),
    property_square_meters double precision,
    property_images character varying(10000),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    property_description character varying(10000),
    property_title character varying(5000),
    property_id character varying(998),
    property_video character varying(10000),
    property_map character varying(10000)
);


ALTER TABLE public.properties OWNER TO astinaam;

--
-- Name: properties_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.properties_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.properties_id_seq OWNER TO astinaam;

--
-- Name: properties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.properties_id_seq OWNED BY public.properties.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(191) NOT NULL,
    display_name character varying(191) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO astinaam;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO astinaam;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.settings (
    id integer NOT NULL,
    key character varying(191) NOT NULL,
    display_name character varying(191) NOT NULL,
    value text,
    details text,
    type character varying(191) NOT NULL,
    "order" integer DEFAULT 1 NOT NULL,
    "group" character varying(191)
);


ALTER TABLE public.settings OWNER TO astinaam;

--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.settings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_id_seq OWNER TO astinaam;

--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: translations; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.translations (
    id integer NOT NULL,
    table_name character varying(191) NOT NULL,
    column_name character varying(191) NOT NULL,
    foreign_key integer NOT NULL,
    locale character varying(191) NOT NULL,
    value text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.translations OWNER TO astinaam;

--
-- Name: translations_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.translations_id_seq OWNER TO astinaam;

--
-- Name: translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.translations_id_seq OWNED BY public.translations.id;


--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.user_roles (
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.user_roles OWNER TO astinaam;

--
-- Name: users; Type: TABLE; Schema: public; Owner: astinaam
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(191) NOT NULL,
    email character varying(191) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(191) NOT NULL,
    is_admin boolean DEFAULT false NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    avatar character varying(191) DEFAULT 'users/default.png'::character varying,
    role_id integer,
    settings text
);


ALTER TABLE public.users OWNER TO astinaam;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: astinaam
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO astinaam;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: astinaam
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: data_rows id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.data_rows ALTER COLUMN id SET DEFAULT nextval('public.data_rows_id_seq'::regclass);


--
-- Name: data_types id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.data_types ALTER COLUMN id SET DEFAULT nextval('public.data_types_id_seq'::regclass);


--
-- Name: menu_items id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.menu_items ALTER COLUMN id SET DEFAULT nextval('public.menu_items_id_seq'::regclass);


--
-- Name: menus id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.menus ALTER COLUMN id SET DEFAULT nextval('public.menus_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: pages id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.pages ALTER COLUMN id SET DEFAULT nextval('public.pages_id_seq'::regclass);


--
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- Name: posts id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.posts ALTER COLUMN id SET DEFAULT nextval('public.posts_id_seq'::regclass);


--
-- Name: properties id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.properties ALTER COLUMN id SET DEFAULT nextval('public.properties_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: translations id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.translations ALTER COLUMN id SET DEFAULT nextval('public.translations_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.categories (id, parent_id, "order", name, slug, created_at, updated_at) FROM stdin;
1	\N	1	Category 1	category-1	2018-11-21 23:11:14	2018-11-21 23:11:14
2	\N	1	Category 2	category-2	2018-11-21 23:11:14	2018-11-21 23:11:14
\.


--
-- Data for Name: data_rows; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.data_rows (id, data_type_id, field, type, display_name, required, browse, read, edit, add, delete, details, "order") FROM stdin;
1	1	id	number	ID	t	f	f	f	f	f	\N	1
2	1	name	text	Name	t	t	t	t	t	t	\N	2
3	1	email	text	Email	t	t	t	t	t	t	\N	3
4	1	password	password	Password	t	f	f	t	t	f	\N	4
5	1	remember_token	text	Remember Token	f	f	f	f	f	f	\N	5
6	1	created_at	timestamp	Created At	f	t	t	f	f	f	\N	6
7	1	updated_at	timestamp	Updated At	f	f	f	f	f	f	\N	7
8	1	avatar	image	Avatar	f	t	t	t	t	t	\N	8
9	1	user_belongsto_role_relationship	relationship	Role	f	t	t	t	t	f	{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsTo","column":"role_id","key":"id","label":"display_name","pivot_table":"roles","pivot":0}	10
10	1	user_belongstomany_role_relationship	relationship	Roles	f	t	t	t	t	f	{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsToMany","column":"id","key":"id","label":"display_name","pivot_table":"user_roles","pivot":"1","taggable":"0"}	11
11	1	locale	text	Locale	f	t	t	t	t	f	\N	12
12	1	settings	hidden	Settings	f	f	f	f	f	f	\N	12
13	2	id	number	ID	t	f	f	f	f	f	\N	1
14	2	name	text	Name	t	t	t	t	t	t	\N	2
15	2	created_at	timestamp	Created At	f	f	f	f	f	f	\N	3
16	2	updated_at	timestamp	Updated At	f	f	f	f	f	f	\N	4
17	3	id	number	ID	t	f	f	f	f	f	\N	1
18	3	name	text	Name	t	t	t	t	t	t	\N	2
19	3	created_at	timestamp	Created At	f	f	f	f	f	f	\N	3
20	3	updated_at	timestamp	Updated At	f	f	f	f	f	f	\N	4
21	3	display_name	text	Display Name	t	t	t	t	t	t	\N	5
22	1	role_id	text	Role	t	t	t	t	t	t	\N	9
23	4	id	number	ID	t	f	f	f	f	f	\N	1
24	4	parent_id	select_dropdown	Parent	f	f	t	t	t	t	{"default":"","null":"","options":{"":"-- None --"},"relationship":{"key":"id","label":"name"}}	2
25	4	order	text	Order	t	t	t	t	t	t	{"default":1}	3
26	4	name	text	Name	t	t	t	t	t	t	\N	4
27	4	slug	text	Slug	t	t	t	t	t	t	{"slugify":{"origin":"name"}}	5
28	4	created_at	timestamp	Created At	f	f	t	f	f	f	\N	6
29	4	updated_at	timestamp	Updated At	f	f	f	f	f	f	\N	7
30	5	id	number	ID	t	f	f	f	f	f	\N	1
31	5	author_id	text	Author	t	f	t	t	f	t	\N	2
32	5	category_id	text	Category	t	f	t	t	t	f	\N	3
33	5	title	text	Title	t	t	t	t	t	t	\N	4
34	5	excerpt	text_area	Excerpt	t	f	t	t	t	t	\N	5
35	5	body	rich_text_box	Body	t	f	t	t	t	t	\N	6
36	5	image	image	Post Image	f	t	t	t	t	t	{"resize":{"width":"1000","height":"null"},"quality":"70%","upsize":true,"thumbnails":[{"name":"medium","scale":"50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300","height":"250"}}]}	7
37	5	slug	text	Slug	t	f	t	t	t	t	{"slugify":{"origin":"title","forceUpdate":true},"validation":{"rule":"unique:posts,slug"}}	8
38	5	meta_description	text_area	Meta Description	t	f	t	t	t	t	\N	9
39	5	meta_keywords	text_area	Meta Keywords	t	f	t	t	t	t	\N	10
40	5	status	select_dropdown	Status	t	t	t	t	t	t	{"default":"DRAFT","options":{"PUBLISHED":"published","DRAFT":"draft","PENDING":"pending"}}	11
41	5	created_at	timestamp	Created At	f	t	t	f	f	f	\N	12
42	5	updated_at	timestamp	Updated At	f	f	f	f	f	f	\N	13
43	5	seo_title	text	SEO Title	f	t	t	t	t	t	\N	14
44	5	featured	checkbox	Featured	t	t	t	t	t	t	\N	15
45	6	id	number	ID	t	f	f	f	f	f	\N	1
46	6	author_id	text	Author	t	f	f	f	f	f	\N	2
47	6	title	text	Title	t	t	t	t	t	t	\N	3
48	6	excerpt	text_area	Excerpt	t	f	t	t	t	t	\N	4
49	6	body	rich_text_box	Body	t	f	t	t	t	t	\N	5
50	6	slug	text	Slug	t	f	t	t	t	t	{"slugify":{"origin":"title"},"validation":{"rule":"unique:pages,slug"}}	6
51	6	meta_description	text	Meta Description	t	f	t	t	t	t	\N	7
52	6	meta_keywords	text	Meta Keywords	t	f	t	t	t	t	\N	8
53	6	status	select_dropdown	Status	t	t	t	t	t	t	{"default":"INACTIVE","options":{"INACTIVE":"INACTIVE","ACTIVE":"ACTIVE"}}	9
54	6	created_at	timestamp	Created At	t	t	t	f	f	f	\N	10
55	6	updated_at	timestamp	Updated At	t	f	f	f	f	f	\N	11
56	6	image	image	Page Image	f	t	t	t	t	t	\N	12
57	7	id	text	Id	t	f	f	f	f	f	{}	1
62	7	property_city	text	Property City	f	t	t	t	t	t	{}	9
63	7	property_location	text	Property Location	f	t	t	t	t	t	{}	10
70	7	property_title	text	Property Title	f	t	t	t	t	t	{}	3
71	7	property_id	text	Property Id	f	t	t	t	t	t	{}	2
61	7	property_price	number	Property Price	f	t	t	t	t	t	{}	8
64	7	property_bedrooms	number	Property Bedrooms	f	t	t	t	t	t	{}	11
65	7	property_square_meters	number	Property Square Meters	f	t	t	t	t	t	{}	12
66	7	property_images	multiple_images	Property Images	f	t	t	t	t	t	{}	13
69	7	property_description	text_area	Property Description	f	t	t	t	t	t	{}	6
72	7	property_video	code_editor	Property Video	f	t	t	t	t	t	{}	14
58	7	property_type	text	Property Type	f	t	t	t	t	t	{}	5
59	7	property_status	select_dropdown	Property Status	f	t	t	t	t	t	{"default":"option1","options":{"New":"New","Resale":"Resale","Offplan":"Offplan","Rent":"Rent","Under Construction":"Under Construction"}}	4
67	7	created_at	timestamp	Created At	f	t	t	t	f	t	{}	16
68	7	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	17
73	7	property_map	code_editor	Property Map	f	t	t	t	t	t	{}	15
60	7	property_features	text_area	Property Features	f	t	t	t	t	t	{}	7
\.


--
-- Data for Name: data_types; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.data_types (id, name, slug, display_name_singular, display_name_plural, icon, model_name, description, generate_permissions, created_at, updated_at, server_side, controller, policy_name, details) FROM stdin;
1	users	users	User	Users	voyager-person	TCG\\Voyager\\Models\\User		t	2018-11-21 23:11:12	2018-11-21 23:11:12	0		TCG\\Voyager\\Policies\\UserPolicy	\N
2	menus	menus	Menu	Menus	voyager-list	TCG\\Voyager\\Models\\Menu		t	2018-11-21 23:11:12	2018-11-21 23:11:12	0		\N	\N
3	roles	roles	Role	Roles	voyager-lock	TCG\\Voyager\\Models\\Role		t	2018-11-21 23:11:12	2018-11-21 23:11:12	0		\N	\N
4	categories	categories	Category	Categories	voyager-categories	TCG\\Voyager\\Models\\Category		t	2018-11-21 23:11:13	2018-11-21 23:11:13	0		\N	\N
5	posts	posts	Post	Posts	voyager-news	TCG\\Voyager\\Models\\Post		t	2018-11-21 23:11:14	2018-11-21 23:11:14	0		TCG\\Voyager\\Policies\\PostPolicy	\N
6	pages	pages	Page	Pages	voyager-file-text	TCG\\Voyager\\Models\\Page		t	2018-11-21 23:11:14	2018-11-21 23:11:14	0		\N	\N
7	properties	properties	Property	Properties	\N	App\\Property	\N	t	2018-11-21 23:29:35	2018-11-22 02:15:14	0	\N	\N	{"order_column":null,"order_display_column":null}
\.


--
-- Data for Name: menu_items; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.menu_items (id, menu_id, title, url, target, icon_class, color, parent_id, "order", created_at, updated_at, route, parameters) FROM stdin;
1	1	Dashboard		_self	voyager-boat	\N	\N	1	2018-11-21 23:11:13	2018-11-21 23:11:13	voyager.dashboard	\N
2	1	Media		_self	voyager-images	\N	\N	5	2018-11-21 23:11:13	2018-11-21 23:11:13	voyager.media.index	\N
3	1	Users		_self	voyager-person	\N	\N	3	2018-11-21 23:11:13	2018-11-21 23:11:13	voyager.users.index	\N
4	1	Roles		_self	voyager-lock	\N	\N	2	2018-11-21 23:11:13	2018-11-21 23:11:13	voyager.roles.index	\N
5	1	Tools		_self	voyager-tools	\N	\N	9	2018-11-21 23:11:13	2018-11-21 23:11:13	\N	\N
6	1	Menu Builder		_self	voyager-list	\N	5	10	2018-11-21 23:11:13	2018-11-21 23:11:13	voyager.menus.index	\N
7	1	Database		_self	voyager-data	\N	5	11	2018-11-21 23:11:13	2018-11-21 23:11:13	voyager.database.index	\N
8	1	Compass		_self	voyager-compass	\N	5	12	2018-11-21 23:11:13	2018-11-21 23:11:13	voyager.compass.index	\N
9	1	BREAD		_self	voyager-bread	\N	5	13	2018-11-21 23:11:13	2018-11-21 23:11:13	voyager.bread.index	\N
10	1	Settings		_self	voyager-settings	\N	\N	14	2018-11-21 23:11:13	2018-11-21 23:11:13	voyager.settings.index	\N
11	1	Categories		_self	voyager-categories	\N	\N	8	2018-11-21 23:11:14	2018-11-21 23:11:14	voyager.categories.index	\N
12	1	Posts		_self	voyager-news	\N	\N	6	2018-11-21 23:11:14	2018-11-21 23:11:14	voyager.posts.index	\N
13	1	Pages		_self	voyager-file-text	\N	\N	7	2018-11-21 23:11:14	2018-11-21 23:11:14	voyager.pages.index	\N
14	1	Hooks		_self	voyager-hook	\N	5	13	2018-11-21 23:11:15	2018-11-21 23:11:15	voyager.hooks	\N
15	1	Properties		_self	\N	\N	\N	15	2018-11-21 23:29:35	2018-11-21 23:29:35	voyager.properties.index	\N
16	2	About US	/about	_self	\N	#000000	\N	16	2018-11-21 23:31:08	2018-11-21 23:31:08	\N	
17	2	Why Cyprus	/whycyprus	_self	\N	#000000	\N	17	2018-11-21 23:31:29	2018-11-21 23:31:29	\N	
18	2	Properties	/properties	_self	\N	#000000	\N	18	2018-11-21 23:31:47	2018-11-21 23:31:47	\N	
19	2	News	/news	_self	\N	#000000	\N	19	2018-11-21 23:32:01	2018-11-21 23:32:01	\N	
20	2	Pricing	/pricing	_self	\N	#000000	\N	20	2018-11-21 23:32:14	2018-11-21 23:32:14	\N	
\.


--
-- Data for Name: menus; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.menus (id, name, created_at, updated_at) FROM stdin;
1	admin	2018-11-21 23:11:13	2018-11-21 23:11:13
2	global_menu	2018-11-21 23:30:47	2018-11-21 23:30:47
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2016_01_01_000000_add_voyager_user_fields	1
4	2016_01_01_000000_create_data_types_table	1
5	2016_01_01_000000_create_pages_table	1
6	2016_01_01_000000_create_posts_table	1
7	2016_02_15_204651_create_categories_table	1
8	2016_05_19_173453_create_menu_table	1
9	2016_10_21_190000_create_roles_table	1
10	2016_10_21_190000_create_settings_table	1
11	2016_11_30_135954_create_permission_table	1
12	2016_11_30_141208_create_permission_role_table	1
13	2016_12_26_201236_data_types__add__server_side	1
14	2017_01_13_000000_add_route_to_menu_items_table	1
15	2017_01_14_005015_create_translations_table	1
16	2017_01_15_000000_make_table_name_nullable_in_permissions_table	1
17	2017_03_06_000000_add_controller_to_data_types_table	1
18	2017_04_11_000000_alter_post_nullable_fields_table	1
19	2017_04_21_000000_add_order_to_data_rows_table	1
20	2017_07_05_210000_add_policyname_to_data_types_table	1
21	2017_08_05_000000_add_group_to_settings_table	1
22	2017_11_26_013050_add_user_role_relationship	1
23	2017_11_26_015000_create_user_roles_table	1
24	2018_03_11_000000_add_user_settings	1
25	2018_03_14_000000_add_details_to_data_types_table	1
26	2018_03_16_000000_make_settings_value_nullable	1
27	2018_11_07_193729_create_properties_table	1
28	2018_11_16_184156_add_property_description_to_properties	1
29	2018_11_19_224623_add_property_title_to_properties	1
30	2018_11_19_233954_add_property_id_to_properties	1
31	2018_11_20_011947_add_property_video_to_properties	1
32	2018_11_20_013116_add_property_map_to_properties	1
33	2018_11_21_234632_add_property_map_to_properties	2
\.


--
-- Data for Name: pages; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.pages (id, author_id, title, excerpt, body, image, slug, meta_description, meta_keywords, status, created_at, updated_at) FROM stdin;
1	0	Hello World	Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.	<p>Hello World. Scallywag grog swab Cat o'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>	pages/page1.jpg	hello-world	Yar Meta Description	Keyword1, Keyword2	ACTIVE	2018-11-21 23:11:14	2018-11-21 23:11:14
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: permission_role; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.permission_role (permission_id, role_id) FROM stdin;
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
11	1
12	1
13	1
14	1
15	1
16	1
17	1
18	1
19	1
20	1
21	1
22	1
23	1
24	1
25	1
26	1
27	1
28	1
29	1
30	1
31	1
32	1
33	1
34	1
35	1
36	1
37	1
38	1
39	1
40	1
42	1
43	1
44	1
45	1
46	1
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.permissions (id, key, table_name, created_at, updated_at) FROM stdin;
1	browse_admin	\N	2018-11-21 23:11:13	2018-11-21 23:11:13
2	browse_bread	\N	2018-11-21 23:11:13	2018-11-21 23:11:13
3	browse_database	\N	2018-11-21 23:11:13	2018-11-21 23:11:13
4	browse_media	\N	2018-11-21 23:11:13	2018-11-21 23:11:13
5	browse_compass	\N	2018-11-21 23:11:13	2018-11-21 23:11:13
6	browse_menus	menus	2018-11-21 23:11:13	2018-11-21 23:11:13
7	read_menus	menus	2018-11-21 23:11:13	2018-11-21 23:11:13
8	edit_menus	menus	2018-11-21 23:11:13	2018-11-21 23:11:13
9	add_menus	menus	2018-11-21 23:11:13	2018-11-21 23:11:13
10	delete_menus	menus	2018-11-21 23:11:13	2018-11-21 23:11:13
11	browse_roles	roles	2018-11-21 23:11:13	2018-11-21 23:11:13
12	read_roles	roles	2018-11-21 23:11:13	2018-11-21 23:11:13
13	edit_roles	roles	2018-11-21 23:11:13	2018-11-21 23:11:13
14	add_roles	roles	2018-11-21 23:11:13	2018-11-21 23:11:13
15	delete_roles	roles	2018-11-21 23:11:13	2018-11-21 23:11:13
16	browse_users	users	2018-11-21 23:11:13	2018-11-21 23:11:13
17	read_users	users	2018-11-21 23:11:13	2018-11-21 23:11:13
18	edit_users	users	2018-11-21 23:11:13	2018-11-21 23:11:13
19	add_users	users	2018-11-21 23:11:13	2018-11-21 23:11:13
20	delete_users	users	2018-11-21 23:11:13	2018-11-21 23:11:13
21	browse_settings	settings	2018-11-21 23:11:13	2018-11-21 23:11:13
22	read_settings	settings	2018-11-21 23:11:13	2018-11-21 23:11:13
23	edit_settings	settings	2018-11-21 23:11:13	2018-11-21 23:11:13
24	add_settings	settings	2018-11-21 23:11:13	2018-11-21 23:11:13
25	delete_settings	settings	2018-11-21 23:11:13	2018-11-21 23:11:13
26	browse_categories	categories	2018-11-21 23:11:14	2018-11-21 23:11:14
27	read_categories	categories	2018-11-21 23:11:14	2018-11-21 23:11:14
28	edit_categories	categories	2018-11-21 23:11:14	2018-11-21 23:11:14
29	add_categories	categories	2018-11-21 23:11:14	2018-11-21 23:11:14
30	delete_categories	categories	2018-11-21 23:11:14	2018-11-21 23:11:14
31	browse_posts	posts	2018-11-21 23:11:14	2018-11-21 23:11:14
32	read_posts	posts	2018-11-21 23:11:14	2018-11-21 23:11:14
33	edit_posts	posts	2018-11-21 23:11:14	2018-11-21 23:11:14
34	add_posts	posts	2018-11-21 23:11:14	2018-11-21 23:11:14
35	delete_posts	posts	2018-11-21 23:11:14	2018-11-21 23:11:14
36	browse_pages	pages	2018-11-21 23:11:14	2018-11-21 23:11:14
37	read_pages	pages	2018-11-21 23:11:14	2018-11-21 23:11:14
38	edit_pages	pages	2018-11-21 23:11:14	2018-11-21 23:11:14
39	add_pages	pages	2018-11-21 23:11:14	2018-11-21 23:11:14
40	delete_pages	pages	2018-11-21 23:11:14	2018-11-21 23:11:14
41	browse_hooks	\N	2018-11-21 23:11:15	2018-11-21 23:11:15
42	browse_properties	properties	2018-11-21 23:29:35	2018-11-21 23:29:35
43	read_properties	properties	2018-11-21 23:29:35	2018-11-21 23:29:35
44	edit_properties	properties	2018-11-21 23:29:35	2018-11-21 23:29:35
45	add_properties	properties	2018-11-21 23:29:35	2018-11-21 23:29:35
46	delete_properties	properties	2018-11-21 23:29:35	2018-11-21 23:29:35
\.


--
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.posts (id, author_id, category_id, title, seo_title, excerpt, body, image, slug, meta_description, meta_keywords, status, featured, created_at, updated_at) FROM stdin;
1	0	\N	Lorem Ipsum Post	\N	This is the excerpt for the Lorem Ipsum Post	<p>This is the body of the lorem ipsum post</p>	posts/post1.jpg	lorem-ipsum-post	This is the meta description	keyword1, keyword2, keyword3	PUBLISHED	f	2018-11-21 23:11:14	2018-11-21 23:11:14
2	0	\N	My Sample Post	\N	This is the excerpt for the sample Post	<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>	posts/post2.jpg	my-sample-post	Meta Description for sample post	keyword1, keyword2, keyword3	PUBLISHED	f	2018-11-21 23:11:14	2018-11-21 23:11:14
3	0	\N	Latest Post	\N	This is the excerpt for the latest post	<p>This is the body for the latest post</p>	posts/post3.jpg	latest-post	This is the meta description	keyword1, keyword2, keyword3	PUBLISHED	f	2018-11-21 23:11:14	2018-11-21 23:11:14
4	0	\N	Yarr Post	\N	Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.	<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>	posts/post4.jpg	yarr-post	this be a meta descript	keyword1, keyword2, keyword3	PUBLISHED	f	2018-11-21 23:11:14	2018-11-21 23:11:14
\.


--
-- Data for Name: properties; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.properties (id, property_type, property_status, property_features, property_price, property_city, property_location, property_bedrooms, property_square_meters, property_images, created_at, updated_at, property_description, property_title, property_id, property_video, property_map) FROM stdin;
1	Apartment	New	Partly furnished	85000	Paphos	Paphos	1	63.0499999999999972	["properties\\/November2018\\/O9JYicUV9A1xDv650Alv.jpg"]	2018-11-22 01:25:00	2018-11-22 02:16:53	This second-floor apartment is in a small complex only 300m from the beach near the Tomb of the Kings. It has an open plan living dining area with access to a covered veranda with sea views.\r\nThe apartment shares a communal pool and is within a short reach to many amenities and local attractions.	1 Bedroom Apartment For Sale In Paphos	MLS-12614	<iframe width="560" height="315" src="https://www.youtube.com/embed/9aX9qz3Qadw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d52437.10147856252!2d32.397990890696185!3d34.77274779097854!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14e70663f42de2c9%3A0x6c05fdff50f4b5e7!2sPaphos%2C+Cyprus!5e0!3m2!1sen!2sbd!4v1542849906374" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
7	Apartment	New	\N	125000	Nicosia	Ayios Dometios, Nicosia	2	88	["properties\\/November2018\\/pfeUlc6VLIfyEV0ENIZ1.jpg","properties\\/November2018\\/feYqA77rzbth3VcB1LLM.jpg","properties\\/November2018\\/dnLrWTEdoJKo91RDAOps.jpg"]	2018-11-22 01:59:00	2018-11-22 02:17:22	The building is located in a peaceful but also a rapidly developing area where new luxurious buildings are being raised.\r\n\r\nThis project consists of 9 spacious and functional one and two bedroom apartments.	2 Bedroom Apartment For Sale In Ayios Dometios, Nicosia	MLS-11100	\N	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13045.081717580051!2d33.321225620718096!3d35.174811774606056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14de10a1492935ab%3A0x4044a1e9fd176582!2sAgios+Dometios%2C+Cyprus!5e0!3m2!1sen!2sbd!4v1542851988193" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
6	Apartment	New	\N	998	Limassol	Kolossi, Limassol	1	220	["properties\\/November2018\\/GRqH1Si6aaLzsURerEqU.jpg","properties\\/November2018\\/MhZj0NLDI9beB7eC48Hv.jpg","properties\\/November2018\\/rSkXOEZr8IRF8gYkQ6N5.jpg","properties\\/November2018\\/lo9NCvNl3Enj1RrxOSYX.jpg","properties\\/November2018\\/K7sF2yRWgWgEEXqo4NS2.jpg"]	2018-11-22 01:58:00	2018-11-22 02:17:27	Fully furnished apartment situated in Kolossi, Limassol. It has large veranda, an extra kitchen, and an attic.There is a A/C in all areas.	3 Bedroom Apartment For Rent In Kolossi, Limassol	RTL-11053	\N	\N
5	Commercial Building	New	Ideal Location, Raised Floor, Security System, Tourist Area, VRV Air Conditioning System	5000	Limassol	City Center Limassol	2	201	["properties\\/November2018\\/SKP5Kt7vBB8FAc9o3SHj.jpg","properties\\/November2018\\/LotQOPfFpnrHseNY3e7s.jpg","properties\\/November2018\\/60HIDmKbCnE5kjbhANoA.jpg","properties\\/November2018\\/V3nLm2UMiOWOuB2iPSLL.jpg","properties\\/November2018\\/iZz6IgTV9xlXbh77vZn0.jpg","properties\\/November2018\\/COQAUZhfCAkScM76jlI8.jpg"]	2018-11-22 01:56:00	2018-11-22 02:17:32	This is an ultra-modern luxury office located in the heart of tourist area in Limassol, opposite from the sea with excellent sea view.	Offices For Rent In City Center Limasso	CRT-11841	\N	\N
4	Luxury Villas	New	Air Conditioning in all Areas, Central Heating Diesel, Double Glazing, Electrical Appliances, Granite Kitchen Worktops, Laundry Room, Sauna	3000	Limassol	Agios Tychonas, Limass	3	725	["properties\\/November2018\\/B2Er6IGknyWVzyoLBCE1.jpg","properties\\/November2018\\/DIF0T7Nq0lUY4kIFguhT.jpg","properties\\/November2018\\/uhHEOD118XmoosgwK84m.jpg","properties\\/November2018\\/HcIFi1MKNgxglLzu4hPC.jpg","properties\\/November2018\\/bEVldTW7K2fnqjWZJNoa.jpg","properties\\/November2018\\/H5CBstbL88snpvGs5T0b.jpg","properties\\/November2018\\/8N5HN6LFkVopaKuQr2Ku.jpg","properties\\/November2018\\/MdZqaTZwRTRmKyLxGJry.jpg","properties\\/November2018\\/syydOsvNoruIOQRZz1qw.jpg","properties\\/November2018\\/Q2Ny2QwIs6QPsqzkmaKV.jpg","properties\\/November2018\\/sb6G8rEfqdMnx8TPXPeV.jpg","properties\\/November2018\\/ZTcKEjwM6VirqHiGtANv.jpg","properties\\/November2018\\/A9driZgbxZ2aNaxI260c.jpg","properties\\/November2018\\/rkNFmDl9TEwboNHMXKoU.jpg","properties\\/November2018\\/1IgNDAIyBSTRfD35XPxh.jpg","properties\\/November2018\\/0PFtFZSwoK3YPHPF6MZ7.jpg","properties\\/November2018\\/8HI5EyGXbIx1ExcL41ut.jpg","properties\\/November2018\\/74IeyXjFo2A2v43qpiiD.jpg"]	2018-11-22 01:48:00	2018-11-22 02:17:37	The villa is located on a high hill in Ayios Tychonas, and it is ten minutes’ drive to the town’s center. It is set in a quiet residential road, only meters away from the coastal avenue and its exciting nightlife and sandy beaches, while at the same time being within easy access to the highway linking all main towns on the island. The luxury villa comprises 3 levels, consist of 4 bedrooms, master bedroom with en-suite bathroom and walk-in closets, 5 bathrooms, big basement with natural light included spa room with sauna, hammam, billiard room 3 covered parking etc.\r\nThe price include garden and pool maintenance.	4 Bedroom Villa For Rent In Agios Tychonas, Limassol	LUR-12213	<iframe width="560" height="315" src="https://www.youtube.com/embed/9aX9qz3Qadw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d52468.68150834363!2d33.09838329027149!3d34.72301843669411!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14e0ca52b76c298d%3A0x742fceb985fddd37!2sAgios+Tychon%2C+Cyprus!5e0!3m2!1sen!2sbd!4v1542851322819" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
3	Apartment	New	Swimming Pool, Fully furnished, Air Conditioning, Sea view, Issued, Covered Parking	60000	Paphos	Kissonerga, Paphos	1	66	["properties\\/November2018\\/Cyyt8lBAYOMBFGo2pKqM.jpg","properties\\/November2018\\/GtjbrHetpEA2TXO8E8Y3.jpg","properties\\/November2018\\/R2lM9xeXUmcdiKH2vMXL.jpg","properties\\/November2018\\/vRwNQLx96m1eSa91quM7.jpg","properties\\/November2018\\/p26v8YHojM7t3w6vf8ek.jpg","properties\\/November2018\\/30W4aoF0gqOCmXnH8fDA.jpg"]	2018-11-22 01:43:00	2018-11-22 02:17:42	The apartment is surrounded by panoramic Mediterranean Sea view and only 1.5km distance from the sea. Its located in a quiet area in Kissonerga and has a covered parking\r\nIt is on the 3rd floor of the complex, and it is fully furnished and also has a fully fitted and equipped kitchen.	1 Bedroom Apartment For Sale In Kissonerga, Paphos	MLS-12609	\N	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d52397.44488157927!2d32.36452849122956!3d34.835107483598364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14e707d29558104b%3A0x4eee387fc96086a6!2sKissonerga%2C+Cyprus!5e0!3m2!1sen!2sbd!4v1542851008790" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
2	Apartment	New	Issued	69000	Paphos	Pano Paphos	1	58	["properties\\/November2018\\/MWt0Q7fsr2vzIxpbqwxQ.jpg","properties\\/November2018\\/8E81kjf18qg63ftq9EzO.jpg"]	2018-11-22 01:28:00	2018-11-22 02:17:46	Located on the first floor of a small complex in Pano Paphos. Thethe one side fo the complex leads into a cul-de-sac making it both peaceful and semi-private. The bedroom is fitted with laminated flooring, and there is an open plan living-dining area. The kitchen has also been equipped with contemporary fittings.	1 Bedroom Apartment For Sale In Pano Paphos	MLS-12612	\N	\N
9	Commercial Building	Rent	\N	24000000	Limassol	Limassol	\N	400	\N	2018-11-22 02:05:00	2018-11-22 02:15:53	The Building is located in a Commercial Avenue in Limassol and near to the junction leading to the new port.\r\nThe building sits on a plot of 521m² with additional parking on an adjacent plot of 261 m², it has a basement, a ground floor and mezzanine level shop and three floors of office	Commercial Building For Sale In Limassol	11057	\N	\N
11	Commercial Building	New	\N	1400000	Limassol	Gemasogia, Limassol	\N	415	["properties\\/November2018\\/twnsAchepvasYXO7X3wZ.jpg","properties\\/November2018\\/CDYURrzHC55VIr1CvlW2.jpg","properties\\/November2018\\/BjaEOwSmsB5hmDqhCGeN.jpg","properties\\/November2018\\/n76H7ZWiEMO1EKduCzl6.jpg","properties\\/November2018\\/PaAY5Fj9zOW5SMsCvXHL.jpg","properties\\/November2018\\/wrV7S5lm4yosicQrLVFb.jpg"]	2018-11-22 02:09:00	2018-11-22 02:16:58	The shop is positioned in the heart of Limassol in a tourist area.\r\nLong term rent received from licensed franchise Café, situated in the centre of the tourist area in Germasogia, Limassol\r\nTwo shops turned into one with a total covered area of 255 square meters, and the exclusive right to use approx. 160 square meters veranda.	Shop For Sale In Gemasogia, Limassol	COM-11859	<iframe width="560" height="315" src="https://www.youtube.com/embed/9aX9qz3Qadw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	\N
10	Commercial Building	New	\N	4000	Nicosia	Nicosia	\N	363	["properties\\/November2018\\/oTPO7Uqvo718Gw4YYbGs.jpg","properties\\/November2018\\/F86UlAPpAU6t8Ue15MzX.jpg","properties\\/November2018\\/MegjTJdMn1ArZ2tTRGwW.jpg","properties\\/November2018\\/mmsekMNtWarFGAgBb3ws.jpg","properties\\/November2018\\/knyBKIl8426FAdt8BIdj.jpg","properties\\/November2018\\/Q3udwRoyxpHwBLNMsZKQ.jpg","properties\\/November2018\\/ZdC1lTE1hejchb0vFp9S.jpg","properties\\/November2018\\/NNiNHVcHp4x4akqQY76m.jpg","properties\\/November2018\\/v6ZsSvgVs85W8WXHY9su.jpg","properties\\/November2018\\/8qO9P7qrEH1LIhhydwTh.jpg","properties\\/November2018\\/StG8qaT2CnczGqElXvlY.jpg","properties\\/November2018\\/SN8nGKmPvOMi4wvHKd5u.jpg","properties\\/November2018\\/9Qnssy82vCYTtoIrUDwq.jpg"]	2018-11-22 02:07:00	2018-11-22 02:17:04	Spacious office space for sale or rent in Nicosia. It’s within walking distance to shops, coffee shops, bank shops etc. It has raised floors, air conditioning units, 4 toilets, kitchen, security and automation system, 6 private parking spaces for the employees and a lot private parking spaces for the customers. It can be rented as two separated offices for the half price.	Offices for Sale In Rent, Nicosia	11618	\N	\N
8	Apartment	New	Air Conditioning in all Bedrooms, Double Glazing, Elevator, Fully Fitted Kitchen, Granite Kitchen Worktops, Large Veranda, Laundry Room, Pressurised Water System, Storage Room, TV And Telephone Points in all Areas, Under Floor Heating ,Walking Distance to Bus Service, Walking Distance to School,Walking Distance to Shops	458900	Nicosia	Lykavitos, Nicosia	3	270	["properties\\/November2018\\/CVLCXlivIY6P4nNTn7aK.jpg","properties\\/November2018\\/DkSsR7V5xRNaXcJRbQEm.jpg","properties\\/November2018\\/DqZeESmoYRxFoz8ii17M.jpg","properties\\/November2018\\/Dnglun7noPzzV4vVKo2B.jpg","properties\\/November2018\\/yar5o8qLpt9d7TxMVLfo.jpg"]	2018-11-22 02:03:00	2018-11-22 02:17:17	• Spacious 3 Bedroom apartment with quality finish throughout.\r\n• Centrally located in the Lykavitos Area of Nicosia with instant access to all services amenities.\r\n• With modern architectural design, storage facilities with private covered parking.\r\n• Spectacular city views.\r\n• 30 minutes to Larnaca International Airport, easy access to university and colleges.\r\n• Title deeds available.\r\n\r\nThe apartment buildings’ location is at Lykavitos area, in the heart of Nicosia. Due to its great location, there is easy access to all directions within Nicosia. Lykavitos is one of the most central location areas in the capital of Cyprus. The main features of these properties comprise of spacious two and three bedroom apartments with their private covered parking spaces and storage rooms as well as the modern architectural design of the building. They definitely make a great choice for comfortable living in the capital of Cyprus.\r\n\r\nDelivery Date: 3 months after signing the contracts.\r\n\r\nWireless Security System: Provision for Wireless Security System\r\nVideo Phone Door: Provision for Video Phone Door\r\nSatellite Dish: Provision for Satellite Dish\r\nParking: Electric Parking Gate	3 Bedroom Apartment For Sale In Lykavitos, Nicosia	MLS-5886	\N	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13045.081717580051!2d33.321225620718096!3d35.174811774606056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14de10a1492935ab%3A0x4044a1e9fd176582!2sAgios+Dometios%2C+Cyprus!5e0!3m2!1sen!2sbd!4v1542851988193" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.roles (id, name, display_name, created_at, updated_at) FROM stdin;
1	admin	Administrator	2018-11-21 23:11:13	2018-11-21 23:11:13
2	user	Normal User	2018-11-21 23:11:13	2018-11-21 23:11:13
\.


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.settings (id, key, display_name, value, details, type, "order", "group") FROM stdin;
1	site.title	Site Title	Site Title		text	1	Site
2	site.description	Site Description	Site Description		text	2	Site
3	site.logo	Site Logo			image	3	Site
4	site.google_analytics_tracking_id	Google Analytics Tracking ID			text	4	Site
5	admin.bg_image	Admin Background Image			image	5	Admin
6	admin.title	Admin Title	Voyager		text	1	Admin
7	admin.description	Admin Description	Welcome to Voyager. The Missing Admin for Laravel		text	2	Admin
8	admin.loader	Admin Loader			image	3	Admin
9	admin.icon_image	Admin Icon Image			image	4	Admin
10	admin.google_analytics_client_id	Google Analytics Client ID (used for admin dashboard)			text	1	Admin
\.


--
-- Data for Name: translations; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.translations (id, table_name, column_name, foreign_key, locale, value, created_at, updated_at) FROM stdin;
1	data_types	display_name_singular	5	pt	Post	2018-11-21 23:11:14	2018-11-21 23:11:14
2	data_types	display_name_singular	6	pt	Página	2018-11-21 23:11:14	2018-11-21 23:11:14
3	data_types	display_name_singular	1	pt	Utilizador	2018-11-21 23:11:14	2018-11-21 23:11:14
4	data_types	display_name_singular	4	pt	Categoria	2018-11-21 23:11:14	2018-11-21 23:11:14
5	data_types	display_name_singular	2	pt	Menu	2018-11-21 23:11:14	2018-11-21 23:11:14
6	data_types	display_name_singular	3	pt	Função	2018-11-21 23:11:14	2018-11-21 23:11:14
7	data_types	display_name_plural	5	pt	Posts	2018-11-21 23:11:14	2018-11-21 23:11:14
8	data_types	display_name_plural	6	pt	Páginas	2018-11-21 23:11:14	2018-11-21 23:11:14
9	data_types	display_name_plural	1	pt	Utilizadores	2018-11-21 23:11:14	2018-11-21 23:11:14
10	data_types	display_name_plural	4	pt	Categorias	2018-11-21 23:11:14	2018-11-21 23:11:14
11	data_types	display_name_plural	2	pt	Menus	2018-11-21 23:11:14	2018-11-21 23:11:14
12	data_types	display_name_plural	3	pt	Funções	2018-11-21 23:11:14	2018-11-21 23:11:14
13	categories	slug	1	pt	categoria-1	2018-11-21 23:11:14	2018-11-21 23:11:14
14	categories	name	1	pt	Categoria 1	2018-11-21 23:11:14	2018-11-21 23:11:14
15	categories	slug	2	pt	categoria-2	2018-11-21 23:11:14	2018-11-21 23:11:14
16	categories	name	2	pt	Categoria 2	2018-11-21 23:11:14	2018-11-21 23:11:14
17	pages	title	1	pt	Olá Mundo	2018-11-21 23:11:14	2018-11-21 23:11:14
18	pages	slug	1	pt	ola-mundo	2018-11-21 23:11:14	2018-11-21 23:11:14
19	pages	body	1	pt	<p>Olá Mundo. Scallywag grog swab Cat o'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>	2018-11-21 23:11:14	2018-11-21 23:11:14
20	menu_items	title	1	pt	Painel de Controle	2018-11-21 23:11:14	2018-11-21 23:11:14
21	menu_items	title	2	pt	Media	2018-11-21 23:11:15	2018-11-21 23:11:15
22	menu_items	title	12	pt	Publicações	2018-11-21 23:11:15	2018-11-21 23:11:15
23	menu_items	title	3	pt	Utilizadores	2018-11-21 23:11:15	2018-11-21 23:11:15
24	menu_items	title	11	pt	Categorias	2018-11-21 23:11:15	2018-11-21 23:11:15
25	menu_items	title	13	pt	Páginas	2018-11-21 23:11:15	2018-11-21 23:11:15
26	menu_items	title	4	pt	Funções	2018-11-21 23:11:15	2018-11-21 23:11:15
27	menu_items	title	5	pt	Ferramentas	2018-11-21 23:11:15	2018-11-21 23:11:15
28	menu_items	title	6	pt	Menus	2018-11-21 23:11:15	2018-11-21 23:11:15
29	menu_items	title	7	pt	Base de dados	2018-11-21 23:11:15	2018-11-21 23:11:15
30	menu_items	title	10	pt	Configurações	2018-11-21 23:11:15	2018-11-21 23:11:15
\.


--
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.user_roles (user_id, role_id) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: astinaam
--

COPY public.users (id, name, email, email_verified_at, password, is_admin, remember_token, created_at, updated_at, avatar, role_id, settings) FROM stdin;
1	Admin	admin@admin.com	\N	$2y$10$NJS.vQ4qz4x.7EhxHKkXcue/.7PZ0O0WRUvRM0e/j/bYZSIvGnjoq	f	fSW7zWvZMxneCvW8Yxg6PYp9X6jsEDmZKj70SoNiKhtKu7AXgEw8mbld2dqJ	2018-11-21 23:11:14	2018-11-21 23:11:14	users/default.png	1	\N
\.


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.categories_id_seq', 2, true);


--
-- Name: data_rows_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.data_rows_id_seq', 73, true);


--
-- Name: data_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.data_types_id_seq', 7, true);


--
-- Name: menu_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.menu_items_id_seq', 20, true);


--
-- Name: menus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.menus_id_seq', 2, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.migrations_id_seq', 33, true);


--
-- Name: pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.pages_id_seq', 1, true);


--
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.permissions_id_seq', 46, true);


--
-- Name: posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.posts_id_seq', 4, true);


--
-- Name: properties_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.properties_id_seq', 11, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.roles_id_seq', 2, true);


--
-- Name: settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.settings_id_seq', 10, true);


--
-- Name: translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.translations_id_seq', 30, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astinaam
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: categories categories_slug_unique; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_slug_unique UNIQUE (slug);


--
-- Name: data_rows data_rows_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.data_rows
    ADD CONSTRAINT data_rows_pkey PRIMARY KEY (id);


--
-- Name: data_types data_types_name_unique; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.data_types
    ADD CONSTRAINT data_types_name_unique UNIQUE (name);


--
-- Name: data_types data_types_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.data_types
    ADD CONSTRAINT data_types_pkey PRIMARY KEY (id);


--
-- Name: data_types data_types_slug_unique; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.data_types
    ADD CONSTRAINT data_types_slug_unique UNIQUE (slug);


--
-- Name: menu_items menu_items_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.menu_items
    ADD CONSTRAINT menu_items_pkey PRIMARY KEY (id);


--
-- Name: menus menus_name_unique; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.menus
    ADD CONSTRAINT menus_name_unique UNIQUE (name);


--
-- Name: menus menus_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.menus
    ADD CONSTRAINT menus_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: pages pages_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (id);


--
-- Name: pages pages_slug_unique; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_slug_unique UNIQUE (slug);


--
-- Name: permission_role permission_role_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_pkey PRIMARY KEY (permission_id, role_id);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: posts posts_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: posts posts_slug_unique; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_slug_unique UNIQUE (slug);


--
-- Name: properties properties_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.properties
    ADD CONSTRAINT properties_pkey PRIMARY KEY (id);


--
-- Name: roles roles_name_unique; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_name_unique UNIQUE (name);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: settings settings_key_unique; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_key_unique UNIQUE (key);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: translations translations_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.translations
    ADD CONSTRAINT translations_pkey PRIMARY KEY (id);


--
-- Name: translations translations_table_name_column_name_foreign_key_locale_unique; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.translations
    ADD CONSTRAINT translations_table_name_column_name_foreign_key_locale_unique UNIQUE (table_name, column_name, foreign_key, locale);


--
-- Name: user_roles user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (user_id, role_id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: astinaam
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: permission_role_permission_id_index; Type: INDEX; Schema: public; Owner: astinaam
--

CREATE INDEX permission_role_permission_id_index ON public.permission_role USING btree (permission_id);


--
-- Name: permission_role_role_id_index; Type: INDEX; Schema: public; Owner: astinaam
--

CREATE INDEX permission_role_role_id_index ON public.permission_role USING btree (role_id);


--
-- Name: permissions_key_index; Type: INDEX; Schema: public; Owner: astinaam
--

CREATE INDEX permissions_key_index ON public.permissions USING btree (key);


--
-- Name: user_roles_role_id_index; Type: INDEX; Schema: public; Owner: astinaam
--

CREATE INDEX user_roles_role_id_index ON public.user_roles USING btree (role_id);


--
-- Name: user_roles_user_id_index; Type: INDEX; Schema: public; Owner: astinaam
--

CREATE INDEX user_roles_user_id_index ON public.user_roles USING btree (user_id);


--
-- Name: categories categories_parent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES public.categories(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: data_rows data_rows_data_type_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.data_rows
    ADD CONSTRAINT data_rows_data_type_id_foreign FOREIGN KEY (data_type_id) REFERENCES public.data_types(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: menu_items menu_items_menu_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.menu_items
    ADD CONSTRAINT menu_items_menu_id_foreign FOREIGN KEY (menu_id) REFERENCES public.menus(id) ON DELETE CASCADE;


--
-- Name: permission_role permission_role_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: permission_role permission_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: user_roles user_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: user_roles user_roles_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: users users_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: astinaam
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- PostgreSQL database dump complete
--

