<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('property_type')->nullable(true);
            $table->string('property_status')->nullable(true);
            $table->string('property_features')->nullable(true);
            $table->double('property_price')->nullable(true);
            $table->string('property_city')->nullable(true);
            $table->string('property_location')->nullable(true);
            $table->string('property_bedrooms')->nullable(true);
            $table->double('property_square_meters')->nullable(true);
            $table->string('property_images')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
