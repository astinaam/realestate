<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistanceToProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function ( $table) {
            $table->string('property_dist_from_airport')->nullable();
            $table->string('property_dist_from_sea')->nullable();
            $table->string('property_dist_from_port')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function ( $table) {
            $table->dropColumn('property_dist_from_airport');
            $table->dropColumn('property_dist_from_sea');
            $table->dropColumn('property_dist_from_port');
        });
    }
}
